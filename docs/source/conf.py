#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Sphinx documentation generation configuration for ``pymora``."""

import pathlib
import sys
sys.path.insert(0, str(pathlib.Path(__file__).parent.parent.parent.parent))

project = "pymora"
project_copyright = "2023, Dominic Adam Walters"
author = "Dominic Adam Walters"
release = "0.1"

extensions = [
  "sphinx.ext.autodoc",
  "sphinx.ext.autodoc.typehints",
  "sphinx.ext.doctest",
  "sphinx.ext.duration",
  "sphinx.ext.intersphinx",
  "sphinx.ext.viewcode",
]

html_theme = "sphinx_rtd_theme"

intersphinx_mapping = {
    "python": (
        "https://docs.python.org/3",
        None,
    ),
}

# Autodoc Typehints
# always_document_param_types = True  # TODO this forces properties as attrs
typehints_use_signature = True
typehints_use_signature_return = True

# # Autogen
autoclass_content = "class"
autodoc_class_signature = "separated"
# autodoc_default_options = {
#     "members": True,
#     "member-order": ,
#     "undoc-members": ,
#     "private-members": ,
#     "special-members": True,
#     "inherited-members": True,
#     "show-inheritance": ,
#     "ignore-module-all": ,
#     "imported-members": True,
#     "exclude-members": ,
#     "class-doc-from": ,
#     "no-value": ,
# }
autodoc_typehints = "description"
autodoc_typehints_description_target = "all"
autodoc_typehints_format = "short"
