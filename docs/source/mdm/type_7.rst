.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType7
--------

Types
.....

.. autoclass:: pymora.mdm.MDMType7.Preamble
.. autoclass:: pymora.mdm.MDMType7.AckCode
.. autoclass:: pymora.mdm.MDMType7.Version
.. autoclass:: pymora.mdm.MDMType7.SequenceNumber
.. autoclass:: pymora.mdm.MDMType7.ML2BUniqueID
.. autoclass:: pymora.mdm.MDMType7.MDMType
.. autoclass:: pymora.mdm.MDMType7.NumberOfMessages
.. autoclass:: pymora.mdm.MDMType7.ResourceID
.. autoclass:: pymora.mdm.MDMType7.FieldIndicator
.. autoclass:: pymora.mdm.MDMType7.IP
.. autoclass:: pymora.mdm.MDMType7.MAC
.. autoclass:: pymora.mdm.MDMType7.UDPPort

Constants: Indicies
...................

.. autoattribute:: pymora.mdm.MDMType7.PREAMBLE_INDEX
.. autoattribute:: pymora.mdm.MDMType7.ACK_CODE_INDEX
.. autoattribute:: pymora.mdm.MDMType7.VERSION_INDEX
.. autoattribute:: pymora.mdm.MDMType7.SEQUENCE_NUMBER_INDEX
.. autoattribute:: pymora.mdm.MDMType7.ORIGINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType7.DESTINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType7.MDM_TYPE_INDEX
.. autoattribute:: pymora.mdm.MDMType7.NUMBER_OF_MESSAGES_INDEX
.. autoattribute:: pymora.mdm.MDMType7.SWITCH_GROUP_RESOURCE_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType7.USER_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType7.COMMAND_FIELD_INDICATOR_INDEX
.. autoattribute:: pymora.mdm.MDMType7.COMMAND_MUI_INDEX
.. autoattribute:: pymora.mdm.MDMType7.COMMAND_IP_INDEX
.. autoattribute:: pymora.mdm.MDMType7.COMMAND_MAC_INDEX
.. autoattribute:: pymora.mdm.MDMType7.COMMAND_PORT_INDEX
.. autoattribute:: pymora.mdm.MDMType7.CONTEXT_FIELD_INDICATOR_INDEX
.. autoattribute:: pymora.mdm.MDMType7.CONTEXT_MUI_INDEX
.. autoattribute:: pymora.mdm.MDMType7.CONTEXT_IP_INDEX
.. autoattribute:: pymora.mdm.MDMType7.CONTEXT_MAC_INDEX
.. autoattribute:: pymora.mdm.MDMType7.CONTEXT_PORT_INDEX

Constants: Valid Values
.......................

.. autoattribute:: pymora.mdm.MDMType7.PREAMBLE_VALID_VALUE
.. autoattribute:: pymora.mdm.MDMType7.ACK_CODE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType7.VERSION_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType7.MDM_TYPE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType7.NUMBER_OF_MESSAGES_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType7.FIELD_INDICATOR_VALID_VALUES
