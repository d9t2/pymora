.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType5Controller
------------------

Constructors
............

.. automethod:: pymora.mdm.MDMType5Controller.__init__
.. automethod:: pymora.mdm.MDMType5Controller.new

Dunders
.......

.. automethod:: pymora.mdm.MDMType5Controller.__eq__
.. automethod:: pymora.mdm.MDMType5Controller.__getitem__
.. automethod:: pymora.mdm.MDMType5Controller.__setitem__
.. automethod:: pymora.mdm.MDMType5Controller.__str__

Attributes
..........

.. autoattribute:: pymora.mdm.MDMType5Controller.check_for_errors

Properties
..........

.. autoproperty:: pymora.mdm.MDMType5Controller._preamble
.. autoproperty:: pymora.mdm.MDMType5Controller.ack_code
.. autoproperty:: pymora.mdm.MDMType5Controller.version
.. autoproperty:: pymora.mdm.MDMType5Controller.sequence_number
.. autoproperty:: pymora.mdm.MDMType5Controller.origination_id
.. autoproperty:: pymora.mdm.MDMType5Controller.destination_id
.. autoproperty:: pymora.mdm.MDMType5Controller._mdm_type
.. autoproperty:: pymora.mdm.MDMType5Controller._number_of_messages
.. autoproperty:: pymora.mdm.MDMType5Controller.alert_type
.. autoproperty:: pymora.mdm.MDMType5Controller.port_id
.. autoproperty:: pymora.mdm.MDMType5Controller.operational_parameter
.. autoproperty:: pymora.mdm.MDMType5Controller.operational_state
.. autoproperty:: pymora.mdm.MDMType5Controller.parameter_condition

Methods
.......

.. automethod:: pymora.mdm.MDMType5Controller.invalidate_cache
.. automethod:: pymora.mdm.MDMType5Controller.to_dictionary
