.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType6Controller
------------------

Constructors
............

.. automethod:: pymora.mdm.MDMType6Controller.__init__
.. automethod:: pymora.mdm.MDMType6Controller.new

Dunders
.......

.. automethod:: pymora.mdm.MDMType6Controller.__eq__
.. automethod:: pymora.mdm.MDMType6Controller.__getitem__
.. automethod:: pymora.mdm.MDMType6Controller.__setitem__
.. automethod:: pymora.mdm.MDMType6Controller.__str__

Attributes
..........

.. autoattribute:: pymora.mdm.MDMType6Controller.check_for_errors

Properties
..........

.. autoproperty:: pymora.mdm.MDMType6Controller._preamble
.. autoproperty:: pymora.mdm.MDMType6Controller.ack_code
.. autoproperty:: pymora.mdm.MDMType6Controller.version
.. autoproperty:: pymora.mdm.MDMType6Controller.sequence_number
.. autoproperty:: pymora.mdm.MDMType6Controller.origination_id
.. autoproperty:: pymora.mdm.MDMType6Controller.destination_id
.. autoproperty:: pymora.mdm.MDMType6Controller._mdm_type
.. autoproperty:: pymora.mdm.MDMType6Controller._number_of_messages
.. autoproperty:: pymora.mdm.MDMType6Controller.port_id
.. autoproperty:: pymora.mdm.MDMType6Controller.command
.. autoproperty:: pymora.mdm.MDMType6Controller.configuration
.. autoproperty:: pymora.mdm.MDMType6Controller.waveform_operation

Methods
.......

.. automethod:: pymora.mdm.MDMType6Controller.invalidate_cache
.. automethod:: pymora.mdm.MDMType6Controller.to_dictionary
