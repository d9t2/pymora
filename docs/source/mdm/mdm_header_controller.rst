.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMHeaderController
-------------------

Constructors
............

.. automethod:: pymora.mdm.MDMHeaderController.__init__
.. automethod:: pymora.mdm.MDMHeaderController._new

Dunders
.......

.. automethod:: pymora.mdm.MDMHeaderController.__eq__
.. automethod:: pymora.mdm.MDMHeaderController.__getitem__
.. automethod:: pymora.mdm.MDMHeaderController.__setitem__
.. automethod:: pymora.mdm.MDMHeaderController.__str__

Attributes
..........

.. autoattribute:: pymora.mdm.MDMHeaderController.check_for_errors

Properties
..........

.. autoproperty:: pymora.mdm.MDMHeaderController._preamble
.. autoproperty:: pymora.mdm.MDMHeaderController.ack_code
.. autoproperty:: pymora.mdm.MDMHeaderController.version
.. autoproperty:: pymora.mdm.MDMHeaderController.sequence_number
.. autoproperty:: pymora.mdm.MDMHeaderController.origination_id
.. autoproperty:: pymora.mdm.MDMHeaderController.destination_id
.. autoproperty:: pymora.mdm.MDMHeaderController._mdm_type
.. autoproperty:: pymora.mdm.MDMHeaderController._number_of_messages

Methods
.......

.. automethod:: pymora.mdm.MDMHeaderController.invalidate_cache
.. automethod:: pymora.mdm.MDMHeaderController.to_dictionary
