.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType3
--------

Types
.....

.. autoclass:: pymora.mdm.MDMType3.Preamble
.. autoclass:: pymora.mdm.MDMType3.AckCode
.. autoclass:: pymora.mdm.MDMType3.Version
.. autoclass:: pymora.mdm.MDMType3.SequenceNumber
.. autoclass:: pymora.mdm.MDMType3.ML2BUniqueID
.. autoclass:: pymora.mdm.MDMType3.MDMType
.. autoclass:: pymora.mdm.MDMType3.NumberOfMessages
.. autoclass:: pymora.mdm.MDMType3.TimeOfDay

Constants: Indicies
...................

.. autoattribute:: pymora.mdm.MDMType3.PREAMBLE_INDEX
.. autoattribute:: pymora.mdm.MDMType3.ACK_CODE_INDEX
.. autoattribute:: pymora.mdm.MDMType3.VERSION_INDEX
.. autoattribute:: pymora.mdm.MDMType3.SEQUENCE_NUMBER_INDEX
.. autoattribute:: pymora.mdm.MDMType3.ORIGINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType3.DESTINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType3.MDM_TYPE_INDEX
.. autoattribute:: pymora.mdm.MDMType3.NUMBER_OF_MESSAGES_INDEX
.. autoattribute:: pymora.mdm.MDMType3.TIME_OF_DAY_INDEX

Constants: Valid Values
.......................

.. autoattribute:: pymora.mdm.MDMType3.PREAMBLE_VALID_VALUE
.. autoattribute:: pymora.mdm.MDMType3.ACK_CODE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType3.VERSION_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType3.MDM_TYPE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType3.NUMBER_OF_MESSAGES_VALID_VALUES
