.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType4
--------

Types
.....

.. autoclass:: pymora.mdm.MDMType4.Preamble
.. autoclass:: pymora.mdm.MDMType4.AckCode
.. autoclass:: pymora.mdm.MDMType4.Version
.. autoclass:: pymora.mdm.MDMType4.SequenceNumber
.. autoclass:: pymora.mdm.MDMType4.ML2BUniqueID
.. autoclass:: pymora.mdm.MDMType4.MDMType
.. autoclass:: pymora.mdm.MDMType4.NumberOfMessages
.. autoclass:: pymora.mdm.MDMType4.ResourceID
.. autoclass:: pymora.mdm.MDMType4.FieldIndicator
.. autoclass:: pymora.mdm.MDMType4.IP
.. autoclass:: pymora.mdm.MDMType4.MAC
.. autoclass:: pymora.mdm.MDMType4.UDPPort

Constants: Indicies
...................

.. autoattribute:: pymora.mdm.MDMType4.PREAMBLE_INDEX
.. autoattribute:: pymora.mdm.MDMType4.ACK_CODE_INDEX
.. autoattribute:: pymora.mdm.MDMType4.VERSION_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SEQUENCE_NUMBER_INDEX
.. autoattribute:: pymora.mdm.MDMType4.ORIGINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType4.DESTINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType4.MDM_TYPE_INDEX
.. autoattribute:: pymora.mdm.MDMType4.NUMBER_OF_MESSAGES_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SIGNAL_PORT_RESOURCE_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType4.USER_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType4.COMMAND_FIELD_INDICATOR_INDEX
.. autoattribute:: pymora.mdm.MDMType4.COMMAND_MUI_INDEX
.. autoattribute:: pymora.mdm.MDMType4.COMMAND_IP_INDEX
.. autoattribute:: pymora.mdm.MDMType4.COMMAND_MAC_INDEX
.. autoattribute:: pymora.mdm.MDMType4.COMMAND_PORT_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SIGNAL_DATA_FIELD_INDICATOR_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SIGNAL_DATA_MUI_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SIGNAL_DATA_IP_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SIGNAL_DATA_MAC_INDEX
.. autoattribute:: pymora.mdm.MDMType4.SIGNAL_DATA_PORT_INDEX
.. autoattribute:: pymora.mdm.MDMType4.CONTEXT_FIELD_INDICATOR_INDEX
.. autoattribute:: pymora.mdm.MDMType4.CONTEXT_MUI_INDEX
.. autoattribute:: pymora.mdm.MDMType4.CONTEXT_IP_INDEX
.. autoattribute:: pymora.mdm.MDMType4.CONTEXT_MAC_INDEX
.. autoattribute:: pymora.mdm.MDMType4.CONTEXT_PORT_INDEX

Constants: Valid Values
.......................

.. autoattribute:: pymora.mdm.MDMType4.PREAMBLE_VALID_VALUE
.. autoattribute:: pymora.mdm.MDMType4.ACK_CODE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType4.VERSION_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType4.MDM_TYPE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType4.NUMBER_OF_MESSAGES_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType4.FIELD_INDICATOR_VALID_VALUES
