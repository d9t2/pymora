.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType2Controller
------------------

Constructors
............

.. automethod:: pymora.mdm.MDMType2Controller.__init__
.. automethod:: pymora.mdm.MDMType2Controller.new

Dunders
.......

.. automethod:: pymora.mdm.MDMType2Controller.__eq__
.. automethod:: pymora.mdm.MDMType2Controller.__getitem__
.. automethod:: pymora.mdm.MDMType2Controller.__setitem__
.. automethod:: pymora.mdm.MDMType2Controller.__str__

Attributes
..........

.. autoattribute:: pymora.mdm.MDMType2Controller.check_for_errors

Properties
..........

.. autoproperty:: pymora.mdm.MDMType2Controller._preamble
.. autoproperty:: pymora.mdm.MDMType2Controller.ack_code
.. autoproperty:: pymora.mdm.MDMType2Controller.version
.. autoproperty:: pymora.mdm.MDMType2Controller.sequence_number
.. autoproperty:: pymora.mdm.MDMType2Controller.origination_id
.. autoproperty:: pymora.mdm.MDMType2Controller.destination_id
.. autoproperty:: pymora.mdm.MDMType2Controller._mdm_type
.. autoproperty:: pymora.mdm.MDMType2Controller._number_of_messages

Methods
.......

.. automethod:: pymora.mdm.MDMType2Controller.invalidate_cache
.. automethod:: pymora.mdm.MDMType2Controller.to_dictionary
