.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType5
--------

Types
.....

.. autoclass:: pymora.mdm.MDMType5.Preamble
.. autoclass:: pymora.mdm.MDMType5.AckCode
.. autoclass:: pymora.mdm.MDMType5.Version
.. autoclass:: pymora.mdm.MDMType5.SequenceNumber
.. autoclass:: pymora.mdm.MDMType5.ML2BUniqueID
.. autoclass:: pymora.mdm.MDMType5.MDMType
.. autoclass:: pymora.mdm.MDMType5.NumberOfMessages
.. autoclass:: pymora.mdm.MDMType5.AlertType
.. autoclass:: pymora.mdm.MDMType5.PortID
.. autoclass:: pymora.mdm.MDMType5.OperationalParameter
.. autoclass:: pymora.mdm.MDMType5.OperationalState
.. autoclass:: pymora.mdm.MDMType5.ParameterCondition

Constants: Indicies
...................

.. autoattribute:: pymora.mdm.MDMType5.PREAMBLE_INDEX
.. autoattribute:: pymora.mdm.MDMType5.ACK_CODE_INDEX
.. autoattribute:: pymora.mdm.MDMType5.VERSION_INDEX
.. autoattribute:: pymora.mdm.MDMType5.SEQUENCE_NUMBER_INDEX
.. autoattribute:: pymora.mdm.MDMType5.ORIGINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType5.DESTINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMType5.MDM_TYPE_INDEX
.. autoattribute:: pymora.mdm.MDMType5.NUMBER_OF_MESSAGES_INDEX
.. autoattribute:: pymora.mdm.MDMType5.HEALTH_STATUS_1_INDEX
.. autoattribute:: pymora.mdm.MDMType5.OPERATIONAL_STATE_INDEX
.. autoattribute:: pymora.mdm.MDMType5.PARAMETER_CONDITION_INDEX

Constants: Valid Values
.......................

.. autoattribute:: pymora.mdm.MDMType5.PREAMBLE_VALID_VALUE
.. autoattribute:: pymora.mdm.MDMType5.ACK_CODE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType5.VERSION_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType5.MDM_TYPE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType5.NUMBER_OF_MESSAGES_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType5.ALERT_TYPE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType5.OPERATIONAL_PARAMETER_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMType5.PARAMETER_CONDITION_VALID_VALUES
