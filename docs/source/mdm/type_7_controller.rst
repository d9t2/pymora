.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType7Controller
------------------

Constructors
............

.. automethod:: pymora.mdm.MDMType7Controller.__init__
.. automethod:: pymora.mdm.MDMType7Controller.new

Dunders
.......

.. automethod:: pymora.mdm.MDMType7Controller.__eq__
.. automethod:: pymora.mdm.MDMType7Controller.__getitem__
.. automethod:: pymora.mdm.MDMType7Controller.__setitem__
.. automethod:: pymora.mdm.MDMType7Controller.__str__

Attributes
..........

.. autoattribute:: pymora.mdm.MDMType7Controller.check_for_errors

Properties
..........

.. autoproperty:: pymora.mdm.MDMType7Controller._preamble
.. autoproperty:: pymora.mdm.MDMType7Controller.ack_code
.. autoproperty:: pymora.mdm.MDMType7Controller.version
.. autoproperty:: pymora.mdm.MDMType7Controller.sequence_number
.. autoproperty:: pymora.mdm.MDMType7Controller.origination_id
.. autoproperty:: pymora.mdm.MDMType7Controller.destination_id
.. autoproperty:: pymora.mdm.MDMType7Controller._mdm_type
.. autoproperty:: pymora.mdm.MDMType7Controller._number_of_messages
.. autoproperty:: pymora.mdm.MDMType7Controller.switch_group_resource_id
.. autoproperty:: pymora.mdm.MDMType7Controller.user_id
.. autoproperty:: pymora.mdm.MDMType7Controller.command_field_indicator
.. autoproperty:: pymora.mdm.MDMType7Controller.command_mui
.. autoproperty:: pymora.mdm.MDMType7Controller.command_ip
.. autoproperty:: pymora.mdm.MDMType7Controller.command_mac
.. autoproperty:: pymora.mdm.MDMType7Controller.command_port
.. autoproperty:: pymora.mdm.MDMType7Controller.context_field_indicator
.. autoproperty:: pymora.mdm.MDMType7Controller.context_mui
.. autoproperty:: pymora.mdm.MDMType7Controller.context_ip
.. autoproperty:: pymora.mdm.MDMType7Controller.context_mac
.. autoproperty:: pymora.mdm.MDMType7Controller.context_port

Methods
.......

.. automethod:: pymora.mdm.MDMType7Controller.invalidate_cache
.. automethod:: pymora.mdm.MDMType7Controller.to_dictionary
