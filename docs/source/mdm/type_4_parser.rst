.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMType4Parser
--------------

Constructors
............

.. automethod:: pymora.mdm.MDMType4Parser.__init__

Dunders
.......

.. automethod:: pymora.mdm.MDMType4Parser.__eq__
.. automethod:: pymora.mdm.MDMType4Parser.__getitem__
.. automethod:: pymora.mdm.MDMType4Parser.__str__

Attributes
..........

.. autoattribute:: pymora.mdm.MDMType4Parser.check_for_errors

Properties
..........

.. autoproperty:: pymora.mdm.MDMType4Parser._preamble
.. autoproperty:: pymora.mdm.MDMType4Parser.ack_code
.. autoproperty:: pymora.mdm.MDMType4Parser.version
.. autoproperty:: pymora.mdm.MDMType4Parser.sequence_number
.. autoproperty:: pymora.mdm.MDMType4Parser.origination_id
.. autoproperty:: pymora.mdm.MDMType4Parser.destination_id
.. autoproperty:: pymora.mdm.MDMType4Parser._mdm_type
.. autoproperty:: pymora.mdm.MDMType4Parser._number_of_messages
.. autoproperty:: pymora.mdm.MDMType4Parser.signal_port_resource_id
.. autoproperty:: pymora.mdm.MDMType4Parser.user_id
.. autoproperty:: pymora.mdm.MDMType4Parser.command_field_indicator
.. autoproperty:: pymora.mdm.MDMType4Parser.command_mui
.. autoproperty:: pymora.mdm.MDMType4Parser.command_ip
.. autoproperty:: pymora.mdm.MDMType4Parser.command_mac
.. autoproperty:: pymora.mdm.MDMType4Parser.command_port
.. autoproperty:: pymora.mdm.MDMType4Parser.signal_data_field_indicator
.. autoproperty:: pymora.mdm.MDMType4Parser.signal_data_mui
.. autoproperty:: pymora.mdm.MDMType4Parser.signal_data_ip
.. autoproperty:: pymora.mdm.MDMType4Parser.signal_data_mac
.. autoproperty:: pymora.mdm.MDMType4Parser.signal_data_port
.. autoproperty:: pymora.mdm.MDMType4Parser.context_field_indicator
.. autoproperty:: pymora.mdm.MDMType4Parser.context_mui
.. autoproperty:: pymora.mdm.MDMType4Parser.context_ip
.. autoproperty:: pymora.mdm.MDMType4Parser.context_mac
.. autoproperty:: pymora.mdm.MDMType4Parser.context_port

Methods
.......

.. automethod:: pymora.mdm.MDMType4Parser.invalidate_cache
.. automethod:: pymora.mdm.MDMType4Parser.to_dictionary
