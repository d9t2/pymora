.. Copyright (c) 2023 Dominic Adam Walters
.. 
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

MDMHeader
---------

Types
.....

.. autoclass:: pymora.mdm.MDMHeader.Preamble
.. autoclass:: pymora.mdm.MDMHeader.AckCode
.. autoclass:: pymora.mdm.MDMHeader.Version
.. autoclass:: pymora.mdm.MDMHeader.SequenceNumber
.. autoclass:: pymora.mdm.MDMHeader.ML2BUniqueID
.. autoclass:: pymora.mdm.MDMHeader.MDMType
.. autoclass:: pymora.mdm.MDMHeader.NumberOfMessages

Constants: Indicies
...................

.. autoattribute:: pymora.mdm.MDMHeader.PREAMBLE_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.ACK_CODE_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.VERSION_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.SEQUENCE_NUMBER_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.ORIGINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.DESTINATION_ID_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.MDM_TYPE_INDEX
.. autoattribute:: pymora.mdm.MDMHeader.NUMBER_OF_MESSAGES_INDEX

Constants: Valid Values
.......................

.. autoattribute:: pymora.mdm.MDMHeader.PREAMBLE_VALID_VALUE
.. autoattribute:: pymora.mdm.MDMHeader.ACK_CODE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMHeader.VERSION_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMHeader.MDM_TYPE_VALID_VALUES
.. autoattribute:: pymora.mdm.MDMHeader.NUMBER_OF_MESSAGES_VALID_VALUES
