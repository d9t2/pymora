#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a buffer containing an MDM Type 2."""

from typing import cast
from typing_extensions import Self

from ._buffer import Buffer
from ._mdm_header import MDMHeader, MDMHeaderController, MDMHeaderParser


class MDMType2(MDMHeader):
    """Class providing types and constants relating to the MDM Type 2 body."""

    ACK_CODE_VALID_VALUES: list[MDMHeader.AckCode] = [
        0x01,
        0x02,
        0x04,
        0x08,
        0x10,
        0x20,
        0x40,
        0x80,
    ]

    BYTES_IN_PACKET: int = 16


class MDMType2Parser(MDMHeaderParser):
    """Class providing read access to an MDM Type 2 body."""

    def __init__(self, buffer: Buffer, check_for_errors: bool = True):
        """
        Create an MDM Type 2 parser from an existing buffer.

        :param buffer: The buffer to manage.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        :raises ValueError: If ``buffer`` is too small when
                            :attr:`check_for_errors`
        """
        super().__init__(buffer, check_for_errors=check_for_errors)
        if self.check_for_errors:
            if len(self) < MDMType2.BYTES_IN_PACKET:
                raise ValueError("Buffer doesn't have enough bytes in it")

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is the same as ``other``."""
        if not isinstance(other, MDMType2Parser):
            return False
        return (
            super().__eq__(other)
        )

    @property
    def ack_code(self) -> MDMType2.AckCode:
        """
        Acknowledgement code.

        This property cannot be written.
        """
        if not self._ack_code_valid:
            ack_code = self[MDMType2.ACK_CODE_INDEX]
            if self.check_for_errors:
                if ack_code not in MDMType2.ACK_CODE_VALID_VALUES:
                    raise ValueError(f"`{ack_code}` is not a valid ack code")
            self._ack_code_cache = cast(MDMType2.AckCode, ack_code)
            self._ack_code_valid = True
        return self._ack_code_cache


class MDMType2Controller(MDMHeaderController, MDMType2Parser):
    """Class providing read and write access to an MDM Type 2 body."""

    @classmethod
    def new(cls, check_for_errors: bool = True) -> Self:
        """
        Create a MDM type 2 controller from a new ``bytearray``.

        :param check_for_errors: ``True`` if this class should perform
                                 validation.
        """
        ctrllr = super()._new(
            MDMType2.BYTES_IN_PACKET,
            check_for_errors=check_for_errors,
        )
        ctrllr._mdm_type = 2
        ctrllr._number_of_messages = MDMType2.NUMBER_OF_MESSAGES_VALID_VALUES[ctrllr._mdm_type]  # noqa
        return ctrllr

    @property
    def ack_code(self) -> MDMType2.AckCode:
        """
        Acknowledgement code.

        :setter:
        :param value: The new value for :attr:`ack_code`.
        :type value: MDMType2.AckCode
        """
        return MDMType2Parser.ack_code.fget(self)  # type: ignore

    @ack_code.setter
    def ack_code(self, value: MDMType2.AckCode):
        if self.check_for_errors:
            if value not in MDMType2.ACK_CODE_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid ack code")
        self[MDMType2.ACK_CODE_INDEX] = value
        self._ack_code_cache = value
        self._ack_code_valid = True
