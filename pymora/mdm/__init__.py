#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing utilities relating to MORA Data Messages (MDMs)."""

__all__ = [
    # _mdm_header
    "MDMHeader",
    "MDMHeaderController",
    "MDMHeaderParser",
    # _type_2
    "MDMType2",
    "MDMType2Controller",
    "MDMType2Parser",
    # _type_3
    "MDMType3",
    "MDMType3Controller",
    "MDMType3Parser",
    # _type_4
    "MDMType4",
    "MDMType4Controller",
    "MDMType4Parser",
    # _type_5
    "MDMType5",
    "MDMType5Controller",
    "MDMType5Parser",
    # _type_6
    "MDMType6",
    "MDMType6Controller",
    "MDMType6Parser",
    # _type_7
    "MDMType7",
    "MDMType7Controller",
    "MDMType7Parser",
]

from ._mdm_header import MDMHeader, MDMHeaderController, MDMHeaderParser
from ._type_2 import MDMType2, MDMType2Controller, MDMType2Parser
from ._type_3 import MDMType3, MDMType3Controller, MDMType3Parser
from ._type_4 import MDMType4, MDMType4Controller, MDMType4Parser
from ._type_5 import MDMType5, MDMType5Controller, MDMType5Parser
from ._type_6 import MDMType6, MDMType6Controller, MDMType6Parser
from ._type_7 import MDMType7, MDMType7Controller, MDMType7Parser
