#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing functions to manipulate IP addresses."""

import struct
from typing import cast, TypeAlias

from ._buffer import BufferController


IPV4Uint8s: TypeAlias = tuple[int, int, int, int]


def ipv4_uint8s_from_buffer(
    buffer: bytes | bytearray,
    index: int,
) -> IPV4Uint8s:
    """
    Get the bytes of an IPV4 address from a specific place in a bytes buffer.

    :param buffer: Buffer to get an IPV4 address from.
    :param index: The index to get the IPV4 address from.

    :returns: The obtained uint8s of the IPV4 address.
    """
    return cast(IPV4Uint8s, struct.unpack_from(">BBBB", buffer, index))


def ipv4_str_from_ipv4_uint8s(address: IPV4Uint8s) -> str:
    """
    Get a string IPV4 address from a tuple of big endian uint8s.

    :param address: The IPV4 address to represent as a string.

    :returns: A string IPV4 address.
    """
    return ".".join((str(uint8) for uint8 in address))


def ipv4_uint8s_from_ipv4_str(address: str) -> IPV4Uint8s:
    """
    Get a tuple of uint8s IPV4 address from a string.

    :param address: The IPV4 address to represent as a tuple of uint8s.

    :returns: A tuple of uint8s IPV4 address.
    """
    ret = tuple(int(uint8) for uint8 in address.split("."))
    if len(ret) != 4:
        raise ValueError(f"Address `{address}` does not yield 4 uint8s")
    return cast(IPV4Uint8s, ret)


def write_ipv4_uint8s_to_buffer(
    address: IPV4Uint8s,
    buffer: BufferController,
    index: int,
):
    """
    Write a tuple of uint8s representing an IPV4 address into a buffer.

    :param address: The IPV4 address to write into a buffer.
    :param buffer: The buffer to write into.
    :param index: The position to start writing at.
    """
    for i, uint8 in enumerate(address):
        buffer._pack_uint8(uint8, index + i)


def ipv4_str_from_buffer(
    buffer: bytes | bytearray,
    index: int,
) -> str:
    """
    Get a string IPV4 address from a specific place in a bytes buffer.

    :param buffer: Buffer to get an IPV4 address from.
    :param index: The index to get the IPV4 address from.

    :returns: The obtained string IPV4 address.
    """
    return ipv4_str_from_ipv4_uint8s(ipv4_uint8s_from_buffer(buffer, index))


def write_ipv4_str_to_buffer(
    address: str,
    buffer: BufferController,
    index: int,
):
    """
    Write a string representing an IPV4 address into a buffer.

    :param address: The IPV4 address to write into a buffer.
    :param buffer: The buffer to write into.
    :param index: The position to start writing at.
    """
    write_ipv4_uint8s_to_buffer(
        ipv4_uint8s_from_ipv4_str(address),
        buffer,
        index,
    )


IPV6Uint16s: TypeAlias = tuple[int, int, int, int, int, int, int, int]


def ipv6_uint16s_from_buffer(
    buffer: bytes | bytearray,
    index: int,
) -> IPV6Uint16s:
    """
    Get the bytes of an IPV6 address from a specific place in a bytes buffer.

    :param buffer: Buffer to get an IPV6 address from.
    :param index: The index to get the IPV6 address from.

    :returns: The obtained uint16s of the IPV6 address.
    """
    return cast(IPV6Uint16s, struct.unpack_from(">HHHHHHHH", buffer, index))


def ipv6_str_from_ipv6_uint16s(
    address: IPV6Uint16s,
) -> str:
    """
    Get a string IPV6 address from a tuple of big endian uint16s.

    :param address: The IPV6 address to represent as a string.

    :returns: A string IPV6 address.
    """
    return ":".join((f"{uint16:x}" for uint16 in address))


def ipv6_uint16s_from_ipv6_str(address: str) -> IPV6Uint16s:
    """
    Get a tuple of uint16s IPV6 address from a string.

    :param address: The IPV6 address to represent as a tuple of uint8s.

    :returns: A tuple of uint16s IPV6 address.
    """
    ret = tuple(int(uint16, 16) for uint16 in address.split(":"))
    if len(ret) != 8:
        raise ValueError(f"Address `{address}` does not yield 8 uint16s")
    return cast(IPV6Uint16s, ret)


def write_ipv6_uint16s_to_buffer(
    address: IPV6Uint16s,
    buffer: BufferController,
    index: int,
):
    """
    Write a tuple of uint16s representing an IPV6 address into a buffer.

    :param address: The IPV6 address to write into a buffer.
    :param buffer: The buffer to write into.
    :param index: The position to start writing at.
    """
    for i, uint16 in enumerate(address):
        buffer._pack_uint16(uint16, index + 2 * i)


def ipv6_str_from_buffer(
    buffer: bytes | bytearray,
    index: int,
) -> str:
    """
    Get a string IPV6 address from a specific place in a bytes buffer.

    :param buffer: Buffer to get an IPV6 address from.
    :param index: The index to get the IPV6 address from.

    :returns: The obtained string IPV6 address.
    """
    return ipv6_str_from_ipv6_uint16s(ipv6_uint16s_from_buffer(buffer, index))


def write_ipv6_str_to_buffer(
    address: str,
    buffer: BufferController,
    index: int,
):
    """
    Write a string representing an IPV6 address into a buffer.

    :param address: The IPV6 address to write into a buffer.
    :param buffer: The buffer to write into.
    :param index: The position to start writing at.
    """
    write_ipv6_uint16s_to_buffer(
        ipv6_uint16s_from_ipv6_str(address),
        buffer,
        index,
    )
