#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a buffer containing an MDM Type 3."""

import calendar
import time
from typing import Any, TypeAlias
from typing_extensions import Self

from ._buffer import Buffer
from ._mdm_header import MDMHeader, MDMHeaderController, MDMHeaderParser


class MDMType3(MDMHeader):
    """Class providing types and constants relating to the MDM Type 3 body."""

    TimeOfDay: TypeAlias = time.struct_time
    TIME_OF_DAY_INDEX: int = 16

    BYTES_IN_PACKET: int = 20


class MDMType3Parser(MDMHeaderParser):
    """Class providing read access to an MDM Type 3 body."""

    _time_of_day_cache: MDMType3.TimeOfDay
    """Cached time of day."""
    _time_of_day_valid: bool
    """Validity of :attr`:_time_of_day_cache`."""

    def __init__(self, buffer: Buffer, check_for_errors: bool = True):
        """
        Create an MDM Type 3 parser from an existing buffer.

        :param buffer: The buffer to manage.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        :raises ValueError: If ``buffer`` is too small when
                            :attr:`check_for_errors`
        """
        super().__init__(buffer, check_for_errors=check_for_errors)
        if self.check_for_errors:
            if len(self) < MDMType3.BYTES_IN_PACKET:
                raise ValueError("Buffer doesn't have enough bytes in it")
        self._time_of_day_valid = False

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is the same as ``other``."""
        if not isinstance(other, MDMType3Parser):
            return False
        return (
            super().__eq__(other)
            and self.time_of_day == other.time_of_day
        )

    def to_dictionary(self) -> dict[str, Any]:
        """Return a dictionary representation of this object."""
        return {
            **super().to_dictionary(),
            "time_of_day": self.time_of_day,
        }

    @property
    def time_of_day(self) -> MDMType3.TimeOfDay:
        """
        Time of day.

        This property cannot be written.
        """
        if not self._time_of_day_valid:
            time_of_day_int = self._unpack_uint32(
                MDMType3.TIME_OF_DAY_INDEX,
            )
            self._time_of_day_cache = time.gmtime(time_of_day_int)
            self._time_of_day_valid = True
        return self._time_of_day_cache

    def invalidate_cache(self):
        """Invalidate all cached information."""
        super().invalidate_cache()
        self._time_of_day_valid = False


class MDMType3Controller(MDMHeaderController, MDMType3Parser):
    """Class providing read and write access to an MDM Type 3 body."""

    @classmethod
    def new(cls, check_for_errors: bool = True) -> Self:
        """
        Create a MDM type 3 controller from a new ``bytearray``.

        :param check_for_errors: ``True`` if this class should perform
                                 validation.
        """
        ctrllr = super()._new(
            MDMType3.BYTES_IN_PACKET,
            check_for_errors=check_for_errors,
        )
        ctrllr._mdm_type = 3
        ctrllr._number_of_messages = MDMType3.NUMBER_OF_MESSAGES_VALID_VALUES[ctrllr._mdm_type]  # noqa
        return ctrllr

    @property
    def time_of_day(self) -> MDMType3.TimeOfDay:
        """
        Time of day.

        :setter:
        :param value: The new value for :attr:`time_of_day`.
        :type value: int | MDMType3.TimeOfDay
        """
        return MDMType3Parser.time_of_day.fget(self)  # type: ignore

    @time_of_day.setter
    def time_of_day(self, value: int | float | MDMType3.TimeOfDay):
        if isinstance(value, time.struct_time):
            time_of_day_int = calendar.timegm(value)
        elif isinstance(value, int) or isinstance(value, float):
            time_of_day_int = int(value)
            value = time.gmtime(value)
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._pack_uint32(time_of_day_int, MDMType3.TIME_OF_DAY_INDEX)
        if value.tm_zone != "UTC":
            value = time.gmtime(calendar.timegm(value))
        self._time_of_day_cache = value
        self._time_of_day_valid = True
