#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a memory buffer."""

import struct
from typing import cast, Generic, overload, TypeVar
from typing_extensions import Self


Buffer = TypeVar("Buffer", bytes, bytearray)


class BufferParser(Generic[Buffer]):
    """Class providing methods allowing control over an existing bytearray."""

    _buffer: Buffer
    """The internal buffer."""

    def __init__(self, buffer: Buffer, **kwargs):
        """
        Create a buffer parser from an existing buffer.

        :param buffer: The buffer to manage.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        """
        super().__init__(**kwargs)
        if not isinstance(buffer, bytes) and not isinstance(buffer, bytearray):
            raise TypeError("Buffer must be a byte array")
        if not hasattr(self, "_buffer"):
            self._buffer = buffer

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is equal to ``other``."""
        if not isinstance(other, BufferParser):
            return False
        return cast(bool, self._buffer == other._buffer)

    def __len__(self) -> int:
        """Return the length of this buffer."""
        return len(self._buffer)

    def __str__(self) -> str:
        """Return the string representation of this object."""
        return str(self._buffer)

    @overload
    def __getitem__(self, key: int) -> int:
        """``__getitem__`` overload for integer keys."""

    @overload
    def __getitem__(self, key: slice) -> bytearray:
        """``__getitem__`` overload for slice keys."""

    def __getitem__(self, key):
        """
        Evaluate ``self._buffer[key]``.

        This enables read access: ``self[key]``

        :param key: The key to apply to :attr:`_buffer`.
        """
        return self._buffer[key]

    def _unpack_uint8(self, index: int) -> int:
        """
        Unpack an unsigned 8 bit number from the buffer.

        :param index: The index of the byte to start unpacking from.
        """
        return cast(int, struct.unpack(">B", self[index:index])[0])

    def _unpack_uint16(self, index: int) -> int:
        """
        Unpack an unsigned 16 bit number from the buffer.

        :param index: The index of the byte to start unpacking from.
        """
        return cast(int, struct.unpack(">H", self[index:index + 2])[0])

    def _unpack_uint32(self, index: int) -> int:
        """
        Unpack an unsigned 32 bit number from the buffer.

        :param index: The index of the byte to start unpacking from.
        """
        return cast(int, struct.unpack(">I", self[index:index + 4])[0])

    def _unpack_uint64(self, index: int) -> int:
        """
        Unpack an unsigned 64 bit number from the buffer.

        :param index: The index of the byte to start unpacking from.
        """
        return cast(int, struct.unpack(">Q", self[index:index + 8])[0])


class BufferController(BufferParser[bytearray]):
    """Class providing methods allowing control over a new bytearray."""

    def __init__(self, buffer: bytearray, **kwargs):
        """
        Create a buffer parser from an existing ``bytearray``.

        :param buffer: The buffer to manage.

        :raises TypeError: If ``buffer`` isn't a ``bytearray``.
        """
        super().__init__(buffer, **kwargs)
        if not isinstance(buffer, bytearray):
            raise TypeError("Buffer must be mutable byte array")

    @classmethod
    def _new(cls, length: int) -> Self:
        """
        Create a buffer controller from a new ``bytearray``.

        :param length: The length of the buffer to create.
        """
        return cls(bytearray(length))

    @overload
    def __setitem__(self, key: int, value: int):
        """``__setitem__`` overload for integer keys."""

    @overload
    def __setitem__(self, key: slice, value: bytearray):
        """``__setitem__`` overload for slice keys."""

    @overload
    def __setitem__(self, key: slice, value: bytes):
        """``__setitem__`` overload for slice keys."""

    def __setitem__(self, key, value):
        """
        Evaluate ``self._buffer[key] = value``.

        This enables write access: ``self[index] = value``

        :param key: The key to apply to :attr:`_buffer`.
        :param value: The new value to write.
        """
        self._buffer[key] = value

    def _pack_uint8(self, value: int, index: int):
        """
        Pack an unsigned 8 bit number into the buffer.

        :param value: The value to pack into :attr:`_buffer`.
        :param index: The index of the highest byte of ``value``.
        """
        struct.pack_into(">B", self._buffer, index, value)

    def _pack_uint16(self, value: int, index: int):
        """
        Pack an unsigned 16 bit number into the buffer.

        :param value: The value to pack into :attr:`_buffer`.
        :param index: The index of the highest byte of ``value``.
        """
        struct.pack_into(">H", self._buffer, index, value)

    def _pack_uint32(self, value: int, index: int):
        """
        Pack an unsigned 32 bit number into the buffer.

        :param value: The value to pack into :attr:`_buffer`.
        :param index: The index of the highest byte of ``value``.
        """
        struct.pack_into(">I", self._buffer, index, value)

    def _pack_uint64(self, value: int, index: int):
        """
        Pack an unsigned 64 bit number into the buffer.

        :param value: The value to pack into :attr:`_buffer`.
        :param index: The index of the highest byte of ``value``.
        """
        struct.pack_into(">Q", self._buffer, index, value)

    def _pack_uint128(self, value: int, index: int):
        """
        Pack an unsigned 128 bit number into the buffer.

        :param value: The value to pack into :attr:`_buffer`.
        :param index: The index of the highest byte of ``value``.
        """
        high_value = value >> 64
        low_value = value & 0xffffffff_ffffffff
        struct.pack_into(">Q", self._buffer, index, high_value)
        struct.pack_into(">Q", self._buffer, index + 8, low_value)

    def resize(self, length: int, allow_truncation: bool = False):
        """
        Resize this buffer.

        :param length: The new length of :attr:`_buffer`.
        :param allow_truncation: ``True`` if truncation is allowed.

        :raises RuntimeError: If a shrink is attempted without allowing
                              truncation.
        """
        if length < len(self) and not allow_truncation:
            raise RuntimeError("Attempted shrink without allowing truncation")
        if length < len(self):
            self._buffer = self._buffer[:length]
        if length > len(self):
            self._buffer.extend(bytearray(length - len(self)))
