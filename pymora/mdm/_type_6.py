#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a buffer containing an MDM Type 6."""

from typing import Any, cast, get_args, Literal, TypeAlias
from typing_extensions import Self

from ._buffer import Buffer
from ._mdm_header import MDMHeader, MDMHeaderController, MDMHeaderParser


class MDMType6(MDMHeader):
    """Class providing types and constants relating to the MDM Type 6 body."""

    PortID: TypeAlias = int
    Command: TypeAlias = Literal[
        0x0000000,
        0x0000001,
        0x0000002,
        0x0000004,
        0x0000008,
        0x0000010,
        0x0000020,
        0x0000040,
        0x0000080,
        0x0000100,
        0x0000200,
        0x0000400,
    ]
    COMMAND_VALID_VALUES: list[Command] = list(get_args(Command))
    COMMAND_INDEX: int = 16

    Configuration: TypeAlias = int
    CONFIGURATION_INDEX: int = 20

    WaveformOperation: TypeAlias = int
    WAVEFORM_OPERATION_INDEX: int = 24

    BYTES_IN_PACKET: int = 28


class MDMType6Parser(MDMHeaderParser):
    """Class providing read access to an MDM Type 6 body."""

    _port_id_cache: MDMType6.PortID
    """Cached port ID."""
    _port_id_valid: bool
    """Validity of :attr`:_port_id_cache`."""

    _command_cache: MDMType6.Command
    """Cached port ID."""
    _command_valid: bool
    """Validity of :attr`:_command_cache`."""

    _configuration_cache: MDMType6.Configuration
    """Cached configuration."""
    _configuration_valid: bool
    """Validity of :attr`:_configuration_cache`."""

    _waveform_operation_cache: MDMType6.WaveformOperation
    """Cached waveform operation."""
    _waveform_operation_valid: bool
    """Validity of :attr`:_waveform_operation_cache`."""

    def __init__(self, buffer: Buffer, check_for_errors: bool = True):
        """
        Create an MDM Type 6 parser from an existing buffer.

        :param buffer: The buffer to manage.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        :raises ValueError: If ``buffer`` is too small when
                            :attr:`check_for_errors`
        """
        super().__init__(buffer, check_for_errors=check_for_errors)
        if self.check_for_errors:
            if len(self) < MDMType6.BYTES_IN_PACKET:
                raise ValueError("Buffer doesn't have enough bytes in it")
        self._port_id_valid = False
        self._command_valid = False
        self._configuration_valid = False
        self._waveform_operation_valid = False

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is the same as ``other``."""
        if not isinstance(other, MDMType6Parser):
            return False
        return (
            super().__eq__(other)
            and self.port_id == other.port_id
            and self.command == other.command
            and self.configuration == other.configuration
            and self.waveform_operation == other.waveform_operation
        )

    def to_dictionary(self) -> dict[str, Any]:
        """Return a dictionary representation of this object."""
        return {
            **super().to_dictionary(),
            "port_id": self.port_id,
            "command": self.command,
            "configuration": self.configuration,
            "waveform_operation": self.waveform_operation,
        }

    @property
    def port_id(self) -> MDMType6.PortID:
        """
        Port ID.

        This property cannot be written.
        """
        if not self._port_id_valid:
            command_field = self._unpack_uint32(
                MDMType6.COMMAND_INDEX,
            )
            port_id = (command_field >> 25) & 0x0000007f
            self._port_id_cache = port_id
            self._port_id_valid = True
        return self._port_id_cache

    @property
    def command(self) -> MDMType6.Command:
        """
        Command.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._command_valid:
            command_field = self._unpack_uint32(
                MDMType6.COMMAND_INDEX,
            )
            command = command_field & 0x01ffffff
            if self.check_for_errors:
                if command not in MDMType6.COMMAND_VALID_VALUES:
                    raise ValueError(f"`{command}` is not a valid command")
            self._command_cache = cast(
                MDMType6.Command,
                command,
            )
            self._command_valid = True
        return self._command_cache

    @property
    def configuration(self) -> MDMType6.Configuration:
        """
        Configuration.

        This property cannot be written.
        """
        if not self._configuration_valid:
            self._configuration_cache = self._unpack_uint32(
                MDMType6.CONFIGURATION_INDEX,
            )
            self._configuration_valid = True
        return self._configuration_cache

    @property
    def waveform_operation(self) -> MDMType6.WaveformOperation:
        """
        Waveform operation.

        This property cannot be written.
        """
        if not self._waveform_operation_valid:
            self._waveform_operation_cache = self._unpack_uint32(
                MDMType6.WAVEFORM_OPERATION_INDEX,
            )
            self._waveform_operation_valid = True
        return self._waveform_operation_cache

    def invalidate_cache(self):
        """Invalidate all cached information."""
        super().invalidate_cache()
        self._port_id_valid = False
        self._command_valid = False
        self._configuration_valid = False
        self._waveform_operation_valid = False


class MDMType6Controller(MDMHeaderController, MDMType6Parser):
    """Class providing read and write access to an MDM Type 6 body."""

    @classmethod
    def new(cls, check_for_errors: bool = True) -> Self:
        """
        Create a MDM type 6 controller from a new ``bytearray``.

        :param check_for_errors: ``True`` if this class should perform
                                 validation.
        """
        ctrllr = super()._new(
            MDMType6.BYTES_IN_PACKET,
            check_for_errors=check_for_errors,
        )
        ctrllr._mdm_type = 6
        ctrllr._number_of_messages = MDMType6.NUMBER_OF_MESSAGES_VALID_VALUES[ctrllr._mdm_type]  # noqa
        return ctrllr

    @property
    def port_id(self) -> MDMType6.PortID:
        """
        Port ID.

        :setter:
        :param value: The new value for :attr:`port_id`.
        :type value: MDMType6.PortID
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType6Parser.port_id.fget(self)  # type: ignore

    @port_id.setter
    def port_id(self, value: MDMType6.PortID):
        cfe = self.check_for_errors
        self.check_for_errors = False
        command_field = (
            (value << 25)
            + (self.command << 0)
        )
        self.check_for_errors = cfe
        self._pack_uint32(command_field, MDMType6.COMMAND_INDEX)
        self._port_id_cache = value
        self._port_id_valid = True

    @property
    def command(self) -> MDMType6.Command:
        """
        Command.

        :setter:
        :param value: The new value for :attr:`command`.
        :type value: MDMType6.Command
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType6Parser.command.fget(self)  # type: ignore

    @command.setter
    def command(self, value: MDMType6.Command):
        if self.check_for_errors:
            if value not in MDMType6.COMMAND_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid command")
        cfe = self.check_for_errors
        self.check_for_errors = False
        command_field = (
            (self.port_id << 25)
            + (value << 0)
        )
        self.check_for_errors = cfe
        self._pack_uint32(command_field, MDMType6.COMMAND_INDEX)
        self._command_cache = value
        self._command_valid = True

    @property
    def configuration(self) -> MDMType6.Configuration:
        """
        Configuration.

        :setter:
        :param value: The new value for :attr:`configuration`.
        :type value: MDMType6.Configuration
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType6Parser.configuration.fget(self)  # type: ignore

    @configuration.setter
    def configuration(self, value: MDMType6.Configuration):
        self._pack_uint32(value, MDMType6.CONFIGURATION_INDEX)
        self._configuration_cache = value
        self._configuration_valid = True

    @property
    def waveform_operation(self) -> MDMType6.WaveformOperation:
        """
        Waveform operation.

        :setter:
        :param value: The new value for :attr:`waveform_operation`.
        :type value: MDMType6.WaveformOperation
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType6Parser.waveform_operation.fget(self)  # type: ignore

    @waveform_operation.setter
    def waveform_operation(self, value: MDMType6.WaveformOperation):
        self._pack_uint32(value, MDMType6.WAVEFORM_OPERATION_INDEX)
        self._waveform_operation_cache = value
        self._waveform_operation_valid = True
