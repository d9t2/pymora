#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing functions to manipulate MAC addresses."""

import struct
from typing import cast, TypeAlias

from ._buffer import BufferController


MACUint8s: TypeAlias = tuple[int, int, int, int, int, int]


def mac_uint8s_from_buffer(
    buffer: bytes | bytearray,
    index: int,
) -> MACUint8s:
    """
    Get the bytes of a MAC address from a specific place in a bytes buffer.

    :param buffer: Buffer to get a MAC address from.
    :param index: The index to get the MAC address from.

    :returns: The obtained uint8s of the MAC address.
    """
    return cast(MACUint8s, struct.unpack_from(">BBBBBB", buffer, index))


def mac_str_from_mac_uint8s(address: MACUint8s) -> str:
    """
    Get a string MAC address from a tuple of big endian uint8s.

    :param address: The MAC address to represent as a string.

    :returns: A string MAC address.
    """
    return ":".join((f"{uint8:02x}" for uint8 in address))


def mac_uint8s_from_mac_str(address: str) -> MACUint8s:
    """
    Get a tuple of uint8s MAC address from a string.

    :param address: The MAC address to represent as a tuple of uint8s.

    :returns: A tuple of uint8s MAC address.
    """
    ret = tuple(int(uint8, 16) for uint8 in address.split(":"))
    if len(ret) != 6:
        raise ValueError(f"Address `{address}` does not yield 6 uint8s")
    return cast(MACUint8s, ret)


def write_mac_uint8s_to_buffer(
    address: MACUint8s,
    buffer: BufferController,
    index: int,
):
    """
    Write a tuple of uint8s representing a MAC address into a buffer.

    :param address: The MAC address to write into a buffer.
    :param buffer: The buffer to write into.
    :param index: The position to start writing at.
    """
    for i, uint8 in enumerate(address):
        buffer._pack_uint8(uint8, index + i)


def mac_str_from_buffer(
    buffer: bytes | bytearray,
    index: int,
) -> str:
    """
    Get a string MAC address from a specific place in a bytes buffer.

    :param buffer: Buffer to get a MAC address from.
    :param index: The index to get the MAC address from.

    :returns: The obtained string MAC address.
    """
    return mac_str_from_mac_uint8s(mac_uint8s_from_buffer(buffer, index))


def write_mac_str_to_buffer(
    address: str,
    buffer: BufferController,
    index: int,
):
    """
    Write a string representing a MAC address into a buffer.

    :param address: The MAC address to write into a buffer.
    :param buffer: The buffer to write into.
    :param index: The position to start writing at.
    """
    write_mac_uint8s_to_buffer(
        mac_uint8s_from_mac_str(address),
        buffer,
        index,
    )
