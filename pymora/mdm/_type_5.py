#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a buffer containing an MDM Type 5."""

from typing import Any, cast, get_args, Literal, TypeAlias
from typing_extensions import Self

from ._buffer import Buffer
from ._mdm_header import MDMHeader, MDMHeaderController, MDMHeaderParser


class MDMType5(MDMHeader):
    """Class providing types and constants relating to the MDM Type 5 body."""

    AlertType: TypeAlias = Literal[0, 1, 2]
    ALERT_TYPE_VALID_VALUES: list[AlertType] = list(get_args(AlertType))

    PortID: TypeAlias = int

    OperationalParameter: TypeAlias = Literal[
        0x001,
        0x002,
        0x004,
        0x008,
        0x010,
        0x020,
        0x040,
        0x080,
        0x100,
        0x200,
        0x400,
    ]
    OPERATIONAL_PARAMETER_VALID_VALUES: list[OperationalParameter] = list(get_args(OperationalParameter))  # noqa

    HEALTH_STATUS_1_INDEX: int = 16

    OperationalState: TypeAlias = int
    OPERATIONAL_STATE_INDEX: int = 20

    ParameterCondition: TypeAlias = Literal[
        0x001,
        0x002,
        0x004,
        0x008,
        0x010,
        0x020,
        0x040,
        0x080,
        0x100,
        0x200,
        0x400,
        0x800,
    ]
    PARAMETER_CONDITION_VALID_VALUES: list[ParameterCondition] = list(get_args(ParameterCondition))  # noqa
    PARAMETER_CONDITION_INDEX: int = 22

    BYTES_IN_PACKET: int = 24


class MDMType5Parser(MDMHeaderParser):
    """Class providing read access to an MDM Type 5 body."""

    _alert_type_cache: MDMType5.AlertType
    """Cached alert type."""
    _alert_type_valid: bool
    """Validity of :attr`:_alert_type_cache`."""

    _port_id_cache: MDMType5.PortID
    """Cached port ID."""
    _port_id_valid: bool
    """Validity of :attr`:_port_id_cache`."""

    _operational_parameter_cache: MDMType5.OperationalParameter
    """Cached operational parameter."""
    _operational_parameter_valid: bool
    """Validity of :attr`:_operational_parameter_cache`."""

    _operational_state_cache: MDMType5.OperationalState
    """Cached operational state."""
    _operational_state_valid: bool
    """Validity of :attr`:_operational_state_cache`."""

    _parameter_condition_cache: MDMType5.ParameterCondition
    """Cached parameter condition."""
    _parameter_condition_valid: bool
    """Validity of :attr`:_parameter_condition_cache`."""

    def __init__(self, buffer: Buffer, check_for_errors: bool = True):
        """
        Create an MDM Type 5 parser from an existing buffer.

        :param buffer: The buffer to manage.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        :raises ValueError: If ``buffer`` is too small when
                            :attr:`check_for_errors`
        """
        super().__init__(buffer, check_for_errors=check_for_errors)
        if self.check_for_errors:
            if len(self) < MDMType5.BYTES_IN_PACKET:
                raise ValueError("Buffer doesn't have enough bytes in it")
        self._alert_type_valid = False
        self._port_id_valid = False
        self._operational_parameter_valid = False
        self._operational_state_valid = False
        self._parameter_condition_valid = False

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is the same as ``other``."""
        if not isinstance(other, MDMType5Parser):
            return False
        return (
            super().__eq__(other)
            and self.alert_type == other.alert_type
            and self.port_id == other.port_id
            and self.operational_parameter == other.operational_parameter
            and self.operational_state == other.operational_state
            and self.parameter_condition == other.parameter_condition
        )

    def to_dictionary(self) -> dict[str, Any]:
        """Return a dictionary representation of this object."""
        return {
            **super().to_dictionary(),
            "alert_type": self.alert_type,
            "port_id": self.port_id,
            "operational_parameter": self.operational_parameter,
            "operational_state": self.operational_state,
            "parameter_condition": self.parameter_condition,
        }

    @property
    def alert_type(self) -> MDMType5.AlertType:
        """
        Alert type.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._alert_type_valid:
            health_status_1 = self._unpack_uint32(
                MDMType5.HEALTH_STATUS_1_INDEX,
            )
            alert_type = health_status_1 >> 30
            if self.check_for_errors:
                if alert_type not in MDMType5.ALERT_TYPE_VALID_VALUES:
                    raise ValueError(f"`{alert_type}` is not a valid alert "
                                     "type")
            self._alert_type_cache = cast(MDMType5.AlertType, alert_type)
            self._alert_type_valid = True
        return self._alert_type_cache

    @property
    def port_id(self) -> MDMType5.PortID:
        """
        Port ID.

        This property cannot be written.
        """
        if not self._port_id_valid:
            health_status_1 = self._unpack_uint32(
                MDMType5.HEALTH_STATUS_1_INDEX,
            )
            port_id = (health_status_1 >> 23) & 0x0000007f
            self._port_id_cache = port_id
            self._port_id_valid = True
        return self._port_id_cache

    @property
    def operational_parameter(self) -> MDMType5.OperationalParameter:
        """
        Operational parameter.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._operational_parameter_valid:
            health_status_1 = self._unpack_uint32(
                MDMType5.HEALTH_STATUS_1_INDEX,
            )
            operational_parameter = health_status_1 & 0x007fffff
            if self.check_for_errors:
                if operational_parameter not in MDMType5.OPERATIONAL_PARAMETER_VALID_VALUES:  # noqa
                    raise ValueError(f"`{operational_parameter}` is not a "
                                     "valid operational parameter")
            self._operational_parameter_cache = cast(
                MDMType5.OperationalParameter,
                operational_parameter,
            )
            self._operational_parameter_valid = True
        return self._operational_parameter_cache

    @property
    def operational_state(self) -> MDMType5.OperationalState:
        """
        Operational state.

        This property cannot be written.
        """
        if not self._operational_state_valid:
            self._operational_state_cache = self._unpack_uint16(
                MDMType5.OPERATIONAL_STATE_INDEX,
            )
            self._operational_state_valid = True
        return self._operational_state_cache

    @property
    def parameter_condition(self) -> MDMType5.ParameterCondition:
        """
        Parameter condition.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._parameter_condition_valid:
            parameter_condition = self._unpack_uint16(
                MDMType5.PARAMETER_CONDITION_INDEX,
            )
            if self.check_for_errors:
                if parameter_condition not in MDMType5.PARAMETER_CONDITION_VALID_VALUES:  # noqa
                    raise ValueError(f"`{parameter_condition}` is not a valid "
                                     "parameter condition")
            self._parameter_condition_cache = cast(
                MDMType5.ParameterCondition,
                parameter_condition,
            )
            self._parameter_condition_valid = True
        return self._parameter_condition_cache

    def invalidate_cache(self):
        """Invalidate all cached information."""
        super().invalidate_cache()
        self._alert_type_valid = False
        self._port_id_valid = False
        self._operational_parameter_valid = False
        self._operational_state_valid = False
        self._parameter_condition_valid = False


class MDMType5Controller(MDMHeaderController, MDMType5Parser):
    """Class providing read and write access to an MDM Type 5 body."""

    @classmethod
    def new(cls, check_for_errors: bool = True) -> Self:
        """
        Create a MDM type 5 controller from a new ``bytearray``.

        :param check_for_errors: ``True`` if this class should perform
                                 validation.
        """
        ctrllr = super()._new(
            MDMType5.BYTES_IN_PACKET,
            check_for_errors=check_for_errors,
        )
        ctrllr._mdm_type = 5
        ctrllr._number_of_messages = MDMType5.NUMBER_OF_MESSAGES_VALID_VALUES[ctrllr._mdm_type]  # noqa
        return ctrllr

    @property
    def alert_type(self) -> MDMType5.AlertType:
        """
        Alert type.

        :setter:
        :param value: The new value for :attr:`alert_type`.
        :type value: MDMType5.AlertType
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType5Parser.alert_type.fget(self)  # type: ignore

    @alert_type.setter
    def alert_type(self, value: MDMType5.AlertType):
        if self.check_for_errors:
            if value not in MDMType5.ALERT_TYPE_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid operational "
                                 "parameter")
        cfe = self.check_for_errors
        self.check_for_errors = False
        health_status_1 = (
            (value << 30)
            + (self.port_id << 23)
            + (self.operational_parameter << 0)
        )
        self.check_for_errors = cfe
        self._pack_uint32(health_status_1, MDMType5.HEALTH_STATUS_1_INDEX)
        self._alert_type_cache = value
        self._alert_type_valid = True

    @property
    def port_id(self) -> MDMType5.PortID:
        """
        Port ID.

        :setter:
        :param value: The new value for :attr:`port_id`.
        :type value: MDMType5.PortID
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType5Parser.port_id.fget(self)  # type: ignore

    @port_id.setter
    def port_id(self, value: MDMType5.PortID):
        cfe = self.check_for_errors
        self.check_for_errors = False
        health_status_1 = (
            (self.alert_type << 30)
            + (value << 23)
            + (self.operational_parameter << 0)
        )
        self.check_for_errors = cfe
        self._pack_uint32(health_status_1, MDMType5.HEALTH_STATUS_1_INDEX)
        self._port_id_cache = value
        self._port_id_valid = True

    @property
    def operational_parameter(self) -> MDMType5.OperationalParameter:
        """
        Operational parameter.

        :setter:
        :param value: The new value for :attr:`operational_parameter`.
        :type value: MDMType5.OperationalParameter
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType5Parser.operational_parameter.fget(self)  # type: ignore

    @operational_parameter.setter
    def operational_parameter(self, value: MDMType5.OperationalParameter):
        if self.check_for_errors:
            if value not in MDMType5.OPERATIONAL_PARAMETER_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid operational "
                                 "parameter")
        cfe = self.check_for_errors
        self.check_for_errors = False
        health_status_1 = (
            (self.alert_type << 30)
            + (self.port_id << 23)
            + (value << 0)
        )
        self.check_for_errors = cfe
        self._pack_uint32(health_status_1, MDMType5.HEALTH_STATUS_1_INDEX)
        self._operational_parameter_cache = value
        self._operational_parameter_valid = True

    @property
    def operational_state(self) -> MDMType5.OperationalState:
        """
        Operational state.

        :setter:
        :param value: The new value for :attr:`operational_state`.
        :type value: MDMType5.OperationalState
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType5Parser.operational_state.fget(self)  # type: ignore

    @operational_state.setter
    def operational_state(self, value: MDMType5.OperationalState):
        self._pack_uint16(value, MDMType5.OPERATIONAL_STATE_INDEX)
        self._operational_state_cache = value
        self._operational_state_valid = True

    @property
    def parameter_condition(self) -> MDMType5.ParameterCondition:
        """
        Parameter condition.

        :setter:
        :param value: The new value for :attr:`parameter_condition`.
        :type value: MDMType5.ParameterCondition
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType5Parser.parameter_condition.fget(self)  # type: ignore

    @parameter_condition.setter
    def parameter_condition(self, value: MDMType5.ParameterCondition):
        if self.check_for_errors:
            if value not in MDMType5.PARAMETER_CONDITION_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid parameter "
                                 "condition")
        self._pack_uint16(value, MDMType5.PARAMETER_CONDITION_INDEX)
        self._parameter_condition_cache = value
        self._parameter_condition_valid = True
