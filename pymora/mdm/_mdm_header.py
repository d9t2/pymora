#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a buffer containing an MDM header."""

import struct
from typing import Any, cast, get_args, Literal, TypeAlias
from typing_extensions import Self

from ._buffer import Buffer, BufferController, BufferParser


class MDMHeader:
    """Class providing types and constants relating to the MDM header."""

    ML2BUniqueID: TypeAlias = int

    Preamble: TypeAlias = Literal[b"MDM"]
    PREAMBLE_INDEX: int = 0
    PREAMBLE_VALID_VALUE: Preamble = get_args(Preamble)[0]

    AckCode: TypeAlias = Literal[
        0x00,
        0x01,
        0x02,
        0x04,
        0x08,
        0x10,
        0x20,
        0x40,
        0x80,
    ]
    ACK_CODE_INDEX: int = 3
    ACK_CODE_VALID_VALUES: list[AckCode] = [0x00, 0x01]

    Version: TypeAlias = tuple[int, int]
    VERSION_INDEX: int = 4
    VERSION_VALID_VALUES: list[Version] = [(2, 5)]

    SequenceNumber: TypeAlias = int
    SEQUENCE_NUMBER_INDEX: int = 6

    ORIGINATION_ID_INDEX: int = 8

    DESTINATION_ID_INDEX: int = 10

    MDMType: TypeAlias = Literal[1, 2, 3, 4, 5, 6, 7]
    MDM_TYPE_INDEX: int = 12
    MDM_TYPE_VALID_VALUES: list[MDMType] = list(get_args(MDMType))

    NumberOfMessages: TypeAlias = Literal[0, 1]
    NUMBER_OF_MESSAGES_INDEX: int = 14
    NUMBER_OF_MESSAGES_VALID_VALUES: dict[MDMType, NumberOfMessages] = {
        1: 1,
        2: 0,
        3: 1,
        4: 1,
        5: 1,
        6: 1,
        7: 1,
    }

    BYTES_IN_PACKET: int = 16


class MDMHeaderParser(BufferParser):
    """Class providing read access to an MDM header."""

    check_for_errors: bool
    """``True`` if this class should perform validation."""

    _preamble_cache: MDMHeader.Preamble
    """Cached preamble."""
    _preamble_valid: bool
    """Validity of :attr`:_preamble`."""

    _ack_code_cache: MDMHeader.AckCode
    """Cached acknowledgement code."""
    _ack_code_valid: bool
    """Validity of :attr`:_ack_code`."""

    _version_cache: MDMHeader.Version
    """Cached version."""
    _version_valid: bool
    """Validity of :attr`:_version`."""

    _sequence_number_cache: MDMHeader.SequenceNumber
    """Cached acknowledgement code."""
    _sequence_number_valid: bool
    """Validity of :attr`:_sequence_number`."""

    _origination_id_cache: MDMHeader.ML2BUniqueID
    """Cached origination ID."""
    _origination_id_valid: bool
    """Validity of :attr`:_origination_id`."""

    _destination_id_cache: MDMHeader.ML2BUniqueID
    """Cached destination ID."""
    _destination_id_valid: bool
    """Validity of :attr`:_destination_id`."""

    _mdm_type_cache: MDMHeader.MDMType
    """Cached MDM type."""
    _mdm_type_valid: bool
    """Validity of :attr`:_mdm_type`."""

    _number_of_messages_cache: MDMHeader.NumberOfMessages
    """Cached number of messages."""
    _number_of_messages_valid: bool
    """Validity of :attr`:_number_of_messages`."""

    def __init__(self, buffer: Buffer, check_for_errors: bool = True):
        """
        Create an MDM header parser from an existing buffer.

        :param buffer: The buffer to manage.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        :raises ValueError: If ``buffer`` is too small when
                            :attr:`check_for_errors`
        """
        super().__init__(buffer)
        self.check_for_errors = check_for_errors
        if self.check_for_errors:
            if len(self) < MDMHeader.BYTES_IN_PACKET:
                raise ValueError("Buffer doesn't have enough bytes in it")
        self._preamble_valid = False
        self._ack_code_valid = False
        self._version_valid = False
        self._sequence_number_valid = False
        self._origination_id_valid = False
        self._destination_id_valid = False
        self._mdm_type_valid = False
        self._number_of_messages_valid = False

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is the same as ``other``."""
        if not isinstance(other, MDMHeaderParser):
            return False
        return (
            super().__eq__(other)
            and self._preamble == other._preamble
            and self.ack_code == other.ack_code
            and self.version == other.version
            and self.sequence_number == other.sequence_number
            and self.origination_id == other.origination_id
            and self.destination_id == other.destination_id
            and self._mdm_type == other._mdm_type
            and self._number_of_messages == other._number_of_messages
        )

    def to_dictionary(self) -> dict[str, Any]:
        """Return a dictionary representation of this object."""
        return {
            "_buffer": self._buffer,
            "_preamble": self._preamble,
            "ack_code": self.ack_code,
            "version": self.version,
            "sequence_number": self.sequence_number,
            "origination_id": self.origination_id,
            "destination_id": self.destination_id,
            "_mdm_type": self._mdm_type,
            "_number_of_messages": self._number_of_messages,
        }

    def __str__(self) -> str:
        """Return a string representation of this object."""
        return (
            f"{self.__class__.__name__}(\n"
            "    " + ",\n    ".join((f"{k}={v}" for k, v in self.to_dictionary().items())) + ",\n"  # noqa
            ")"
        )

    @property
    def _preamble(self) -> MDMHeader.Preamble:
        """
        First three bytes of an MDM packet.

        This property cannot be written.
        """
        if not self._preamble_valid:
            preamble = self[
                MDMHeader.PREAMBLE_INDEX:
                MDMHeader.PREAMBLE_INDEX + 3
            ]
            if self.check_for_errors:
                if preamble != MDMHeader.PREAMBLE_VALID_VALUE:
                    raise ValueError(f"`{str(preamble)}` is not a valid "
                                     "preamble")
            self._preamble_cache = cast(MDMHeader.Preamble, preamble)
            self._preamble_valid = True
        return self._preamble_cache

    @property
    def ack_code(self) -> MDMHeader.AckCode:
        """
        Acknowledgement code.

        This property cannot be written.
        """
        if not self._ack_code_valid:
            ack_code = self[MDMHeader.ACK_CODE_INDEX]
            if self.check_for_errors:
                if ack_code not in MDMHeader.ACK_CODE_VALID_VALUES:
                    raise ValueError(f"`{ack_code}` is not a valid ack code")
            self._ack_code_cache = cast(MDMHeader.AckCode, ack_code)
            self._ack_code_valid = True
        return self._ack_code_cache

    @property
    def version(self) -> MDMHeader.Version:
        """
        Version.

        This property cannot be written.
        """
        if not self._version_valid:
            version = cast(tuple[int, int], struct.unpack_from(
                ">BB",
                self._buffer,
                MDMHeader.VERSION_INDEX,
            ))
            if self.check_for_errors:
                if version not in MDMHeader.VERSION_VALID_VALUES:
                    raise ValueError(f"`{version}` is not a supported version")
            self._version_cache = version
            self._version_valid = True
        return self._version_cache

    @property
    def sequence_number(self) -> MDMHeader.SequenceNumber:
        """
        Sequence number.

        This property cannot be written.
        """
        if not self._sequence_number_valid:
            self._sequence_number_cache = self._unpack_uint16(
                MDMHeader.SEQUENCE_NUMBER_INDEX,
            )
            self._sequence_number_valid = True
        return self._sequence_number_cache

    @property
    def origination_id(self) -> MDMHeader.ML2BUniqueID:
        """
        Origination ID.

        This property cannot be written.
        """
        if not self._origination_id_valid:
            self._origination_id_cache = self._unpack_uint16(
                MDMHeader.ORIGINATION_ID_INDEX,
            )
            self._origination_id_valid = True
        return self._origination_id_cache

    @property
    def destination_id(self) -> MDMHeader.ML2BUniqueID:
        """
        Destination ID.

        This property cannot be written.
        """
        if not self._destination_id_valid:
            self._destination_id_cache = self._unpack_uint16(
                MDMHeader.DESTINATION_ID_INDEX,
            )
            self._destination_id_valid = True
        return self._destination_id_cache

    @property
    def _mdm_type(self) -> MDMHeader.MDMType:
        """
        MDM type.

        This property cannot be written.
        """
        if not self._mdm_type_valid:
            mdm_type = self._unpack_uint16(
                MDMHeader.MDM_TYPE_INDEX,
            )
            if self.check_for_errors:
                if mdm_type not in MDMHeader.MDM_TYPE_VALID_VALUES:
                    raise ValueError(f"`{mdm_type}` is not a valid MDM type")
            self._mdm_type_cache = cast(MDMHeader.MDMType, mdm_type)
            self._mdm_type_valid = True
        return self._mdm_type_cache

    @property
    def _number_of_messages(self) -> MDMHeader.NumberOfMessages:
        """
        Number of messages.

        This property cannot be written.
        """
        if not self._number_of_messages_valid:
            number_of_messages = self._unpack_uint16(
                MDMHeader.NUMBER_OF_MESSAGES_INDEX,
            )
            if self.check_for_errors:
                if number_of_messages != MDMHeader.NUMBER_OF_MESSAGES_VALID_VALUES[self._mdm_type]:  # noqa
                    raise ValueError(f"`{number_of_messages}` is not the "
                                     "correct number of messages for MDM Type "
                                     f"{self._mdm_type}")
            self._number_of_messages_cache = cast(
                MDMHeader.NumberOfMessages,
                number_of_messages,
            )
            self._number_of_messages_valid = True
        return self._number_of_messages_cache

    def invalidate_cache(self):
        """Invalidate all cached information."""
        self._preamble_valid = False
        self._ack_code_valid = False
        self._version_valid = False
        self._sequence_number_valid = False
        self._origination_id_valid = False
        self._destination_id_valid = False
        self._mdm_type_valid = False
        self._number_of_messages_valid = False


class MDMHeaderController(BufferController, MDMHeaderParser):
    """Class providing read and write access to an MDM header."""

    @classmethod
    def _new(cls, length: int, check_for_errors: bool = True) -> Self:
        """
        Create a MDM header controller from a new ``bytearray``.

        :param length: The length of the buffer to create.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.
        """
        ctrllr = cls(bytearray(length), check_for_errors=check_for_errors)
        ctrllr._preamble = MDMHeader.PREAMBLE_VALID_VALUE
        return ctrllr

    @property
    def _preamble(self) -> MDMHeader.Preamble:
        """
        First three bytes of an MDM packet.

        :setter:
        :param value: The new value for :attr:`_preamble`.
        :type value: MDMHeader.Preamble
        """
        return MDMHeaderParser._preamble.fget(self)  # type: ignore

    @_preamble.setter
    def _preamble(self, value: MDMHeader.Preamble):
        if self.check_for_errors:
            if value != MDMHeader.PREAMBLE_VALID_VALUE:
                raise ValueError(f"`{str(value)}` is not a valid preamble")
        self[
            MDMHeader.PREAMBLE_INDEX:
            MDMHeader.PREAMBLE_INDEX + 3
        ] = value
        self._preamble_cache = value
        self._preamble_valid = True

    @property
    def ack_code(self) -> MDMHeader.AckCode:
        """
        Acknowledgement code.

        :setter:
        :param value: The new value for :attr:`ack_code`.
        :type value: MDMHeader.AckCode
        """
        return MDMHeaderParser.ack_code.fget(self)  # type: ignore

    @ack_code.setter
    def ack_code(self, value: MDMHeader.AckCode):
        if self.check_for_errors:
            if value not in MDMHeader.ACK_CODE_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid ack code")
        self[MDMHeader.ACK_CODE_INDEX] = value
        self._ack_code_cache = value
        self._ack_code_valid = True

    @property
    def version(self) -> MDMHeader.Version:
        """
        Version.

        :setter:
        :param value: The new value for :attr:`version`.
        :type value: MDMHeader.Version
        """
        return MDMHeaderParser.version.fget(self)  # type: ignore

    @version.setter
    def version(self, value: MDMHeader.Version):
        if self.check_for_errors:
            if value not in MDMHeader.VERSION_VALID_VALUES:
                raise ValueError(f"`{value}` is not a supported version")
        struct.pack_into(">2B", self._buffer, MDMHeader.VERSION_INDEX, *value)
        self._version_cache = value
        self._version_valid = True

    @property
    def sequence_number(self) -> MDMHeader.SequenceNumber:
        """
        Sequence number.

        :setter:
        :param value: The new value for :attr:`sequence_number`.
        :type value: MDMHeader.SequenceNumber
        """
        return MDMHeaderParser.sequence_number.fget(self)  # type: ignore

    @sequence_number.setter
    def sequence_number(self, value: MDMHeader.SequenceNumber):
        self._pack_uint16(value, MDMHeader.SEQUENCE_NUMBER_INDEX)
        self._sequence_number_cache = value
        self._sequence_number_valid = True

    @property
    def origination_id(self) -> MDMHeader.ML2BUniqueID:
        """
        Origination ID.

        :setter:
        :param value: The new value for :attr:`origination_id`.
        :type value: MDMHeader.ML2BUniqueID
        """
        return MDMHeaderParser.origination_id.fget(self)  # type: ignore

    @origination_id.setter
    def origination_id(self, value: MDMHeader.ML2BUniqueID):
        self._pack_uint16(value, MDMHeader.ORIGINATION_ID_INDEX)
        self._origination_id_cache = value
        self._origination_id_valid = True

    @property
    def destination_id(self) -> MDMHeader.ML2BUniqueID:
        """
        Destination ID.

        :setter:
        :param value: The new value for :attr:`destination_id`.
        :type value: MDMHeader.ML2BUniqueID
        """
        return MDMHeaderParser.destination_id.fget(self)  # type: ignore

    @destination_id.setter
    def destination_id(self, value: MDMHeader.ML2BUniqueID):
        self._pack_uint16(value, MDMHeader.DESTINATION_ID_INDEX)
        self._destination_id_cache = value
        self._destination_id_valid = True

    @property
    def _mdm_type(self) -> MDMHeader.MDMType:
        """
        MDM type.

        :setter:
        :param value: The new value for :attr:`_mdm_type`.
        :type value: MDMHeader.MDMType
        """
        return MDMHeaderParser._mdm_type.fget(self)  # type: ignore

    @_mdm_type.setter
    def _mdm_type(self, value: MDMHeader.MDMType):
        if self.check_for_errors:
            if value not in MDMHeader.MDM_TYPE_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid MDM type")
        self._pack_uint16(value, MDMHeader.MDM_TYPE_INDEX)
        self._mdm_type_cache = value
        self._mdm_type_valid = True

    @property
    def _number_of_messages(self) -> MDMHeader.NumberOfMessages:
        """
        Number of messages.

        :setter:
        :param value: The new value for :attr:`_number_of_messages`.
        :type value: MDMHeader.NumberOfMessages
        """
        return MDMHeaderParser._number_of_messages.fget(self)  # type: ignore

    @_number_of_messages.setter
    def _number_of_messages(self, value: MDMHeader.NumberOfMessages):
        if self.check_for_errors:
            if value != MDMHeader.NUMBER_OF_MESSAGES_VALID_VALUES[self._mdm_type]:  # noqa
                raise ValueError(f"`{value}` is not the correct number of "
                                 f"messages for MDM Type {self._mdm_type}")
        self._pack_uint16(value, MDMHeader.NUMBER_OF_MESSAGES_INDEX)
        self._number_of_messages_cache = value
        self._number_of_messages_valid = True
