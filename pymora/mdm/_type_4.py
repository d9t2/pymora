#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing control over a buffer containing an MDM Type 4."""

from typing import Any, cast, get_args, Literal, TypeAlias
from typing_extensions import Self

from ._buffer import Buffer
from ._ip import (
    ipv4_str_from_buffer,
    ipv6_str_from_buffer,
    write_ipv4_str_to_buffer,
    write_ipv6_str_to_buffer,
)
from ._mac import mac_str_from_buffer, write_mac_str_to_buffer
from ._mdm_header import MDMHeader, MDMHeaderController, MDMHeaderParser


class MDMType4(MDMHeader):
    """Class providing types and constants relating to the MDM Type 4 body."""

    ResourceID: TypeAlias = int
    FieldIndicator: TypeAlias = Literal[1, 2]
    FIELD_INDICATOR_VALID_VALUES: list[FieldIndicator] = list(get_args(FieldIndicator))  # noqa
    IP: TypeAlias = str
    MAC: TypeAlias = str
    UDPPort: TypeAlias = int

    SIGNAL_PORT_RESOURCE_ID_INDEX: int = 16

    USER_ID_INDEX: int = 18

    COMMAND_FIELD_INDICATOR_INDEX: int = 20
    COMMAND_MUI_INDEX: int = 22
    COMMAND_IP_INDEX: int = 24
    COMMAND_MAC_INDEX: int = 40
    COMMAND_PORT_INDEX: int = 46

    SIGNAL_DATA_FIELD_INDICATOR_INDEX: int = 48
    SIGNAL_DATA_MUI_INDEX: int = 50
    SIGNAL_DATA_IP_INDEX: int = 52
    SIGNAL_DATA_MAC_INDEX: int = 68
    SIGNAL_DATA_PORT_INDEX: int = 74

    CONTEXT_FIELD_INDICATOR_INDEX: int = 76
    CONTEXT_MUI_INDEX: int = 78
    CONTEXT_IP_INDEX: int = 80
    CONTEXT_MAC_INDEX: int = 96
    CONTEXT_PORT_INDEX: int = 102

    BYTES_IN_PACKET: int = 104


class MDMType4Parser(MDMHeaderParser):
    """Class providing read access to an MDM Type 4 body."""

    _signal_port_resource_id_cache: MDMType4.ResourceID
    """Cached signal port user ID."""
    _signal_port_resource_id_valid: bool
    """Validity of :attr`:_signal_port_resource_id_cache`."""

    _user_id_cache: MDMType4.ResourceID
    """Cached user ID."""
    _user_id_valid: bool
    """Validity of :attr`:_user_id_cache`."""

    _command_field_indicator_cache: MDMType4.FieldIndicator
    """Cached command field indicator."""
    _command_field_indicator_valid: bool
    """Validity of :attr`:_command_field_indicator_cache`."""

    _command_mui_cache: MDMType4.ML2BUniqueID
    """Cached command MUI."""
    _command_mui_valid: bool
    """Validity of :attr`:_command_mui_cache`."""

    _command_ip_cache: MDMType4.IP
    """Cached command IP."""
    _command_ip_valid: bool
    """Validity of :attr`:_command_ip_cache`."""

    _command_mac_cache: MDMType4.MAC
    """Cached command MAC."""
    _command_mac_valid: bool
    """Validity of :attr`:_command_mac_cache`."""

    _command_port_cache: MDMType4.UDPPort
    """Cached command port."""
    _command_port_valid: bool
    """Validity of :attr`:_command_port_cache`."""

    _signal_data_field_indicator_cache: MDMType4.FieldIndicator
    """Cached signal_data field indicator."""
    _signal_data_field_indicator_valid: bool
    """Validity of :attr`:_signal_data_field_indicator_cache`."""

    _signal_data_mui_cache: MDMType4.ML2BUniqueID
    """Cached signal_data MUI."""
    _signal_data_mui_valid: bool
    """Validity of :attr`:_signal_data_mui_cache`."""

    _signal_data_ip_cache: MDMType4.IP
    """Cached signal_data IP."""
    _signal_data_ip_valid: bool
    """Validity of :attr`:_signal_data_ip_cache`."""

    _signal_data_mac_cache: MDMType4.MAC
    """Cached signal_data MAC."""
    _signal_data_mac_valid: bool
    """Validity of :attr`:_signal_data_mac_cache`."""

    _signal_data_port_cache: MDMType4.UDPPort
    """Cached signal_data port."""
    _signal_data_port_valid: bool
    """Validity of :attr`:_signal_data_port_cache`."""

    _context_field_indicator_cache: MDMType4.FieldIndicator
    """Cached context field indicator."""
    _context_field_indicator_valid: bool
    """Validity of :attr`:_context_field_indicator_cache`."""

    _context_mui_cache: MDMType4.ML2BUniqueID
    """Cached context MUI."""
    _context_mui_valid: bool
    """Validity of :attr`:_context_mui_cache`."""

    _context_ip_cache: MDMType4.IP
    """Cached context IP."""
    _context_ip_valid: bool
    """Validity of :attr`:_context_ip_cache`."""

    _context_mac_cache: MDMType4.MAC
    """Cached context MAC."""
    _context_mac_valid: bool
    """Validity of :attr`:_context_mac_cache`."""

    _context_port_cache: MDMType4.UDPPort
    """Cached context port."""
    _context_port_valid: bool
    """Validity of :attr`:_context_port_cache`."""

    def __init__(self, buffer: Buffer, check_for_errors: bool = True):
        """
        Create an MDM Type 4 parser from an existing buffer.

        :param buffer: The buffer to manage.
        :param check_for_errors: ``True`` if this class should perform
                                 validation.

        :raises TypeError: If ``buffer`` isn't ``bytes`` or ``bytearray``.
        :raises ValueError: If ``buffer`` is too small when
                            :attr:`check_for_errors`
        """
        super().__init__(buffer, check_for_errors=check_for_errors)
        if self.check_for_errors:
            if len(self) < MDMType4.BYTES_IN_PACKET:
                raise ValueError("Buffer doesn't have enough bytes in it")
        self._signal_port_resource_id_valid = False
        self._user_id_valid = False
        self._command_field_indicator_valid = False
        self._command_mui_valid = False
        self._command_ip_valid = False
        self._command_mac_valid = False
        self._command_port_valid = False
        self._signal_data_field_indicator_valid = False
        self._signal_data_mui_valid = False
        self._signal_data_ip_valid = False
        self._signal_data_mac_valid = False
        self._signal_data_port_valid = False
        self._context_field_indicator_valid = False
        self._context_mui_valid = False
        self._context_ip_valid = False
        self._context_mac_valid = False
        self._context_port_valid = False

    def __eq__(self, other) -> bool:
        """Return ``True`` if this object is the same as ``other``."""
        if not isinstance(other, MDMType4Parser):
            return False
        return (
            super().__eq__(other)
            and self.signal_port_resource_id == other.signal_port_resource_id
            and self.user_id == other.user_id
            and self.command_field_indicator == other.command_field_indicator
            and self.command_mui == other.command_mui
            and self.command_ip == other.command_ip
            and self.command_mac == other.command_mac
            and self.command_port == other.command_port
            and self.signal_data_field_indicator == other.signal_data_field_indicator  # noqa
            and self.signal_data_mui == other.signal_data_mui
            and self.signal_data_ip == other.signal_data_ip
            and self.signal_data_mac == other.signal_data_mac
            and self.signal_data_port == other.signal_data_port
            and self.context_field_indicator == other.context_field_indicator
            and self.context_mui == other.context_mui
            and self.context_ip == other.context_ip
            and self.context_mac == other.context_mac
            and self.context_port == other.context_port
        )

    def to_dictionary(self) -> dict[str, Any]:
        """Return a dictionary representation of this object."""
        return {
            **super().to_dictionary(),
            "signal_port_resource_id": self.signal_port_resource_id,
            "user_id": self.user_id,
            "command_field_indicator": self.command_field_indicator,
            "command_mui": self.command_mui,
            "command_ip": self.command_ip,
            "command_mac": self.command_mac,
            "command_port": self.command_port,
            "signal_data_field_indicator": self.signal_data_field_indicator,
            "signal_data_mui": self.signal_data_mui,
            "signal_data_ip": self.signal_data_ip,
            "signal_data_mac": self.signal_data_mac,
            "signal_data_port": self.signal_data_port,
            "context_field_indicator": self.context_field_indicator,
            "context_mui": self.context_mui,
            "context_ip": self.context_ip,
            "context_mac": self.context_mac,
            "context_port": self.context_port,
        }

    @property
    def signal_port_resource_id(self) -> MDMType4.ResourceID:
        """
        Signal port resource ID.

        This property cannot be written.
        """
        if not self._signal_port_resource_id_valid:
            self._signal_port_resource_id_cache = self._unpack_uint16(
                MDMType4.SIGNAL_PORT_RESOURCE_ID_INDEX,
            )
            self._signal_port_resource_id_valid = True
        return self._signal_port_resource_id_cache

    @property
    def user_id(self) -> MDMType4.ResourceID:
        """
        User ID.

        This property cannot be written.
        """
        if not self._user_id_valid:
            self._user_id_cache = self._unpack_uint16(
                MDMType4.USER_ID_INDEX,
            )
            self._user_id_valid = True
        return self._user_id_cache

    @property
    def command_field_indicator(self) -> MDMType4.FieldIndicator:
        """
        Command field indicator.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._command_field_indicator_valid:
            command_field_indicator = self._unpack_uint16(
                MDMType4.COMMAND_FIELD_INDICATOR_INDEX,
            )
            if self.check_for_errors:
                if command_field_indicator not in MDMType4.FIELD_INDICATOR_VALID_VALUES:  # noqa
                    raise ValueError(f"`{command_field_indicator}` is not a "
                                     "valid command field indicator")
            self._command_field_indicator_cache = cast(
                MDMType4.FieldIndicator,
                command_field_indicator,
            )
            self._command_field_indicator_valid = True
        return self._command_field_indicator_cache

    @property
    def command_mui(self) -> MDMType4.ML2BUniqueID:
        """
        Command MUI.

        This property cannot be written.
        """
        if not self._command_mui_valid:
            self._command_mui_cache = self._unpack_uint16(
                MDMType4.COMMAND_MUI_INDEX,
            )
            self._command_mui_valid = True
        return self._command_mui_cache

    @property
    def command_ip(self) -> MDMType4.IP:
        """
        Command IP.

        This property cannot be written.
        """
        if not self._command_ip_valid:
            self._command_ip_cache = ""
            if self.command_field_indicator == 1:
                self._command_ip_cache = ipv4_str_from_buffer(
                    self._buffer,
                    MDMType4.COMMAND_IP_INDEX,
                )
            if self.command_field_indicator == 2:
                self._command_ip_cache = ipv6_str_from_buffer(
                    self._buffer,
                    MDMType4.COMMAND_IP_INDEX,
                )
            self._command_ip_valid = True
        return self._command_ip_cache

    @property
    def command_mac(self) -> MDMType4.MAC:
        """
        Command MAC.

        This property cannot be written.
        """
        if not self._command_mac_valid:
            self._command_mac_cache = mac_str_from_buffer(
                self._buffer,
                MDMType4.COMMAND_MAC_INDEX,
            )
            self._command_mac_valid = True
        return self._command_mac_cache

    @property
    def command_port(self) -> MDMType4.UDPPort:
        """
        Command port.

        This property cannot be written.
        """
        if not self._command_port_valid:
            self._command_port_cache = self._unpack_uint16(
                MDMType4.COMMAND_PORT_INDEX,
            )
            self._command_port_valid = True
        return self._command_port_cache

    @property
    def signal_data_field_indicator(self) -> MDMType4.FieldIndicator:
        """
        Signal data field indicator.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._signal_data_field_indicator_valid:
            signal_data_field_indicator = self._unpack_uint16(
                MDMType4.SIGNAL_DATA_FIELD_INDICATOR_INDEX,
            )
            if self.check_for_errors:
                if signal_data_field_indicator not in MDMType4.FIELD_INDICATOR_VALID_VALUES:  # noqa
                    raise ValueError(f"`{signal_data_field_indicator}` is not "
                                     "a valid signal_data field indicator")
            self._signal_data_field_indicator_cache = cast(
                MDMType4.FieldIndicator,
                signal_data_field_indicator,
            )
            self._signal_data_field_indicator_valid = True
        return self._signal_data_field_indicator_cache

    @property
    def signal_data_mui(self) -> MDMType4.ML2BUniqueID:
        """
        Signal data MUI.

        This property cannot be written.
        """
        if not self._signal_data_mui_valid:
            self._signal_data_mui_cache = self._unpack_uint16(
                MDMType4.SIGNAL_DATA_MUI_INDEX,
            )
            self._signal_data_mui_valid = True
        return self._signal_data_mui_cache

    @property
    def signal_data_ip(self) -> MDMType4.IP:
        """
        Signal data IP.

        This property cannot be written.
        """
        if not self._signal_data_ip_valid:
            self._signal_data_ip_cache = ""
            if self.signal_data_field_indicator == 1:
                self._signal_data_ip_cache = ipv4_str_from_buffer(
                    self._buffer,
                    MDMType4.SIGNAL_DATA_IP_INDEX,
                )
            if self.signal_data_field_indicator == 2:
                self._signal_data_ip_cache = ipv6_str_from_buffer(
                    self._buffer,
                    MDMType4.SIGNAL_DATA_IP_INDEX,
                )
            self._signal_data_ip_valid = True
        return self._signal_data_ip_cache

    @property
    def signal_data_mac(self) -> MDMType4.MAC:
        """
        Signal data MAC.

        This property cannot be written.
        """
        if not self._signal_data_mac_valid:
            self._signal_data_mac_cache = mac_str_from_buffer(
                self._buffer,
                MDMType4.SIGNAL_DATA_MAC_INDEX,
            )
            self._signal_data_mac_valid = True
        return self._signal_data_mac_cache

    @property
    def signal_data_port(self) -> MDMType4.UDPPort:
        """
        Signal data port.

        This property cannot be written.
        """
        if not self._signal_data_port_valid:
            self._signal_data_port_cache = self._unpack_uint16(
                MDMType4.SIGNAL_DATA_PORT_INDEX,
            )
            self._signal_data_port_valid = True
        return self._signal_data_port_cache

    @property
    def context_field_indicator(self) -> MDMType4.FieldIndicator:
        """
        Context field indicator.

        This property cannot be written.

        :raises ValueError: If the current value of this property is invalid.
        """
        if not self._context_field_indicator_valid:
            context_field_indicator = self._unpack_uint16(
                MDMType4.CONTEXT_FIELD_INDICATOR_INDEX,
            )
            if self.check_for_errors:
                if context_field_indicator not in MDMType4.FIELD_INDICATOR_VALID_VALUES:  # noqa
                    raise ValueError(f"`{context_field_indicator}` is not a "
                                     "valid context field indicator")
            self._context_field_indicator_cache = cast(
                MDMType4.FieldIndicator,
                context_field_indicator,
            )
            self._context_field_indicator_valid = True
        return self._context_field_indicator_cache

    @property
    def context_mui(self) -> MDMType4.ML2BUniqueID:
        """
        Context MUI.

        This property cannot be written.
        """
        if not self._context_mui_valid:
            self._context_mui_cache = self._unpack_uint16(
                MDMType4.CONTEXT_MUI_INDEX,
            )
            self._context_mui_valid = True
        return self._context_mui_cache

    @property
    def context_ip(self) -> MDMType4.IP:
        """
        Context IP.

        This property cannot be written.
        """
        if not self._context_ip_valid:
            self._context_ip_cache = ""
            if self.context_field_indicator == 1:
                self._context_ip_cache = ipv4_str_from_buffer(
                    self._buffer,
                    MDMType4.CONTEXT_IP_INDEX,
                )
            if self.context_field_indicator == 2:
                self._context_ip_cache = ipv6_str_from_buffer(
                    self._buffer,
                    MDMType4.CONTEXT_IP_INDEX,
                )
            self._context_ip_valid = True
        return self._context_ip_cache

    @property
    def context_mac(self) -> MDMType4.MAC:
        """
        Context MAC.

        This property cannot be written.
        """
        if not self._context_mac_valid:
            self._context_mac_cache = mac_str_from_buffer(
                self._buffer,
                MDMType4.CONTEXT_MAC_INDEX,
            )
            self._context_mac_valid = True
        return self._context_mac_cache

    @property
    def context_port(self) -> MDMType4.UDPPort:
        """
        Context port.

        This property cannot be written.
        """
        if not self._context_port_valid:
            self._context_port_cache = self._unpack_uint16(
                MDMType4.CONTEXT_PORT_INDEX,
            )
            self._context_port_valid = True
        return self._context_port_cache

    def invalidate_cache(self):
        """Invalidate all cached information."""
        super().invalidate_cache()
        self._signal_port_resource_id_valid = False
        self._user_id_valid = False
        self._command_field_indicator_valid = False
        self._command_mui_valid = False
        self._command_ip_valid = False
        self._command_mac_valid = False
        self._command_port_valid = False
        self._signal_data_field_indicator_valid = False
        self._signal_data_mui_valid = False
        self._signal_data_ip_valid = False
        self._signal_data_mac_valid = False
        self._signal_data_port_valid = False
        self._context_field_indicator_valid = False
        self._context_mui_valid = False
        self._context_ip_valid = False
        self._context_mac_valid = False
        self._context_port_valid = False


class MDMType4Controller(MDMHeaderController, MDMType4Parser):
    """Class providing read and write access to an MDM Type 4 body."""

    @classmethod
    def new(cls, check_for_errors: bool = True) -> Self:
        """
        Create a MDM type 4 controller from a new ``bytearray``.

        :param check_for_errors: ``True`` if this class should perform
                                 validation.
        """
        ctrllr = super()._new(
            MDMType4.BYTES_IN_PACKET,
            check_for_errors=check_for_errors,
        )
        ctrllr._mdm_type = 4
        ctrllr._number_of_messages = MDMType4.NUMBER_OF_MESSAGES_VALID_VALUES[ctrllr._mdm_type]  # noqa
        return ctrllr

    @property
    def signal_port_resource_id(self) -> MDMType4.ResourceID:
        """
        Signal port resource ID.

        :setter:
        :param value: The new value for :attr:`signal_port_resource_id`.
        :type value: MDMType4.ML2BUniqueID
        """
        return MDMType4Parser.signal_port_resource_id.fget(self)  # type: ignore  # noqa

    @signal_port_resource_id.setter
    def signal_port_resource_id(self, value: MDMType4.ResourceID):
        self._pack_uint16(value, MDMType4.SIGNAL_PORT_RESOURCE_ID_INDEX)
        self._signal_port_resource_id_cache = value
        self._signal_port_resource_id_valid = True

    @property
    def user_id(self) -> MDMType4.ResourceID:
        """
        User ID.

        :setter:
        :param value: The new value for :attr:`user_id`.
        :type value: MDMType4.ML2BUniqueID
        """
        return MDMType4Parser.user_id.fget(self)  # type: ignore

    @user_id.setter
    def user_id(self, value: MDMType4.ResourceID):
        self._pack_uint16(value, MDMType4.USER_ID_INDEX)
        self._user_id_cache = value
        self._user_id_valid = True

    @property
    def command_field_indicator(self) -> MDMType4.FieldIndicator:
        """
        Command field indicator.

        :setter:
        :param value: The new value for :attr:`command_field_indicator`.
        :type value: MDMType4.FieldIndicator
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType4Parser.command_field_indicator.fget(self)  # type: ignore  # noqa

    @command_field_indicator.setter
    def command_field_indicator(self, value: MDMType4.FieldIndicator):
        if self.check_for_errors:
            if value not in MDMType4.FIELD_INDICATOR_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid command field "
                                 "indicator")
        self._pack_uint16(value, MDMType4.COMMAND_FIELD_INDICATOR_INDEX)
        self._command_field_indicator_cache = value
        self._command_field_indicator_valid = True

    @property
    def command_mui(self) -> MDMType4.ML2BUniqueID:
        """
        Command MUI.

        :setter:
        :param value: The new value for :attr:`command_mui`.
        :type value: MDMType4.ML2BUniqueID
        """
        return MDMType4Parser.command_mui.fget(self)  # type: ignore

    @command_mui.setter
    def command_mui(self, value: MDMType4.ML2BUniqueID):
        self._pack_uint16(value, MDMType4.COMMAND_MUI_INDEX)
        self._command_mui_cache = value
        self._command_mui_valid = True

    @property
    def command_ip(self) -> MDMType4.IP:
        """
        Command IP.

        :setter:
        :param value: The new value for :attr:`command_ip`.
        :type value: int | MDMType4.IP
        :raises ValueError: If ``value`` is a string but is badly formatted.
        :raises TypeError: If ``value`` is an invalid type.
        """
        return MDMType4Parser.command_ip.fget(self)  # type: ignore

    @command_ip.setter
    def command_ip(self, value: int | MDMType4.IP):
        if isinstance(value, int):
            if self.command_field_indicator == 1:
                self._pack_uint32(value, MDMType4.COMMAND_IP_INDEX)
            else:  # 2
                self._pack_uint128(value, MDMType4.COMMAND_IP_INDEX)
            self._command_ip_valid = False
            value = self.command_ip
        elif isinstance(value, str):
            if "." in value:
                write_ipv4_str_to_buffer(
                    value,
                    self,
                    MDMType4.COMMAND_IP_INDEX,
                )
                self.command_field_indicator = 1
            elif ":" in value:
                write_ipv6_str_to_buffer(
                    value,
                    self,
                    MDMType4.COMMAND_IP_INDEX,
                )
                self.command_field_indicator = 2
            else:
                raise ValueError(f"Value `{value}` is not an IP address")
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._command_ip_cache = value
        self._command_ip_valid = True

    @property
    def command_mac(self) -> MDMType4.MAC:
        """
        Command MAC.

        :setter:
        :param value: The new value for :attr:`command_mac`.
        :type value: int | MDMType4.MAC
        :raises ValueError: If ``value`` is a string but is badly formatted.
        :raises TypeError: If ``value`` is an invalid type.
        """
        return MDMType4Parser.command_mac.fget(self)  # type: ignore

    @command_mac.setter
    def command_mac(self, value: int | MDMType4.MAC):
        if isinstance(value, int):
            self._pack_uint64(value, MDMType4.COMMAND_MAC_INDEX)
            self._command_mac_valid = False
            value = self.command_mac
        elif isinstance(value, str):
            if ":" in value:
                write_mac_str_to_buffer(
                    value,
                    self,
                    MDMType4.COMMAND_MAC_INDEX,
                )
            else:
                raise ValueError(f"Value `{value}` is not a MAC address")
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._command_mac_cache = value
        self._command_mac_valid = True

    @property
    def command_port(self) -> MDMType4.UDPPort:
        """
        Command port.

        :setter:
        :param value: The new value for :attr:`command_port`.
        :type value: MDMType4.UDPPort
        """
        return MDMType4Parser.command_port.fget(self)  # type: ignore

    @command_port.setter
    def command_port(self, value: MDMType4.UDPPort):
        self._pack_uint16(value, MDMType4.COMMAND_PORT_INDEX)
        self._command_port_cache = value
        self._command_port_valid = True

    @property
    def signal_data_field_indicator(self) -> MDMType4.FieldIndicator:
        """
        Signal data field indicator.

        :setter:
        :param value: The new value for :attr:`signal_data_field_indicator`.
        :type value: MDMType4.FieldIndicator
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType4Parser.signal_data_field_indicator.fget(self)  # type: ignore  # noqa

    @signal_data_field_indicator.setter
    def signal_data_field_indicator(self, value: MDMType4.FieldIndicator):
        if self.check_for_errors:
            if value not in MDMType4.FIELD_INDICATOR_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid signal_data field "
                                 "indicator")
        self._pack_uint16(value, MDMType4.SIGNAL_DATA_FIELD_INDICATOR_INDEX)
        self._signal_data_field_indicator_cache = value
        self._signal_data_field_indicator_valid = True

    @property
    def signal_data_mui(self) -> MDMType4.ML2BUniqueID:
        """
        Signal data MUI.

        :setter:
        :param value: The new value for :attr:`signal_data_mui`.
        :type value: MDMType4.ML2BUniqueID
        """
        return MDMType4Parser.signal_data_mui.fget(self)  # type: ignore

    @signal_data_mui.setter
    def signal_data_mui(self, value: MDMType4.ML2BUniqueID):
        self._pack_uint16(value, MDMType4.SIGNAL_DATA_MUI_INDEX)
        self._signal_data_mui_cache = value
        self._signal_data_mui_valid = True

    @property
    def signal_data_ip(self) -> MDMType4.IP:
        """
        Signal data IP.

        :setter:
        :param value: The new value for :attr:`signal_data_ip`.
        :type value: int | MDMType4.IP
        :raises ValueError: If ``value`` is a string but is badly formatted.
        :raises TypeError: If ``value`` is an invalid type.
        """
        return MDMType4Parser.signal_data_ip.fget(self)  # type: ignore

    @signal_data_ip.setter
    def signal_data_ip(self, value: int | MDMType4.IP):
        if isinstance(value, int):
            if self.signal_data_field_indicator == 1:
                self._pack_uint32(value, MDMType4.SIGNAL_DATA_IP_INDEX)
            else:  # 2
                self._pack_uint128(value, MDMType4.SIGNAL_DATA_IP_INDEX)
            self._signal_data_ip_valid = False
            value = self.signal_data_ip
        elif isinstance(value, str):
            if "." in value:
                write_ipv4_str_to_buffer(
                    value,
                    self,
                    MDMType4.SIGNAL_DATA_IP_INDEX,
                )
                self.signal_data_field_indicator = 1
            elif ":" in value:
                write_ipv6_str_to_buffer(
                    value,
                    self,
                    MDMType4.SIGNAL_DATA_IP_INDEX,
                )
                self.signal_data_field_indicator = 2
            else:
                raise ValueError(f"Value `{value}` is not an IP address")
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._signal_data_ip_cache = value
        self._signal_data_ip_valid = True

    @property
    def signal_data_mac(self) -> MDMType4.MAC:
        """
        Signal data MAC.

        :setter:
        :param value: The new value for :attr:`signal_data_mac`.
        :type value: int | MDMType4.MAC
        :raises ValueError: If ``value`` is a string but is badly formatted.
        :raises TypeError: If ``value`` is an invalid type.
        """
        return MDMType4Parser.signal_data_mac.fget(self)  # type: ignore

    @signal_data_mac.setter
    def signal_data_mac(self, value: int | MDMType4.MAC):
        if isinstance(value, int):
            self._pack_uint64(value, MDMType4.SIGNAL_DATA_MAC_INDEX)
            self._signal_data_mac_valid = False
            value = self.signal_data_mac
        elif isinstance(value, str):
            if ":" in value:
                write_mac_str_to_buffer(
                    value,
                    self,
                    MDMType4.SIGNAL_DATA_MAC_INDEX,
                )
            else:
                raise ValueError(f"Value `{value}` is not a MAC address")
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._signal_data_mac_cache = value
        self._signal_data_mac_valid = True

    @property
    def signal_data_port(self) -> MDMType4.UDPPort:
        """
        Signal data port.

        :setter:
        :param value: The new value for :attr:`signal_data_port`.
        :type value: MDMType4.UDPPort
        """
        return MDMType4Parser.signal_data_port.fget(self)  # type: ignore

    @signal_data_port.setter
    def signal_data_port(self, value: MDMType4.UDPPort):
        self._pack_uint16(value, MDMType4.SIGNAL_DATA_PORT_INDEX)
        self._signal_data_port_cache = value
        self._signal_data_port_valid = True

    @property
    def context_field_indicator(self) -> MDMType4.FieldIndicator:
        """
        Context field indicator.

        :setter:
        :param value: The new value for :attr:`context_field_indicator`.
        :type value: MDMType4.FieldIndicator
        :raises ValueError: If ``value`` is not valid.
        """
        return MDMType4Parser.context_field_indicator.fget(self)  # type: ignore  # noqa

    @context_field_indicator.setter
    def context_field_indicator(self, value: MDMType4.FieldIndicator):
        if self.check_for_errors:
            if value not in MDMType4.FIELD_INDICATOR_VALID_VALUES:
                raise ValueError(f"`{value}` is not a valid context field "
                                 "indicator")
        self._pack_uint16(value, MDMType4.CONTEXT_FIELD_INDICATOR_INDEX)
        self._context_field_indicator_cache = value
        self._context_field_indicator_valid = True

    @property
    def context_mui(self) -> MDMType4.ML2BUniqueID:
        """
        Context MUI.

        :setter:
        :param value: The new value for :attr:`context_mui`.
        :type value: MDMType4.ML2BUniqueID
        """
        return MDMType4Parser.context_mui.fget(self)  # type: ignore

    @context_mui.setter
    def context_mui(self, value: MDMType4.ML2BUniqueID):
        self._pack_uint16(value, MDMType4.CONTEXT_MUI_INDEX)
        self._context_mui_cache = value
        self._context_mui_valid = True

    @property
    def context_ip(self) -> MDMType4.IP:
        """
        Context IP.

        :setter:
        :param value: The new value for :attr:`context_ip`.
        :type value: int | MDMType4.IP
        :raises ValueError: If ``value`` is a string but is badly formatted.
        :raises TypeError: If ``value`` is an invalid type.
        """
        return MDMType4Parser.context_ip.fget(self)  # type: ignore

    @context_ip.setter
    def context_ip(self, value: int | MDMType4.IP):
        if isinstance(value, int):
            if self.context_field_indicator == 1:
                self._pack_uint32(value, MDMType4.CONTEXT_IP_INDEX)
            else:  # 2
                self._pack_uint128(value, MDMType4.CONTEXT_IP_INDEX)
            self._context_ip_valid = False
            value = self.context_ip
        elif isinstance(value, str):
            if "." in value:
                write_ipv4_str_to_buffer(
                    value,
                    self,
                    MDMType4.CONTEXT_IP_INDEX,
                )
                self.context_field_indicator = 1
            elif ":" in value:
                write_ipv6_str_to_buffer(
                    value,
                    self,
                    MDMType4.CONTEXT_IP_INDEX,
                )
                self.context_field_indicator = 2
            else:
                raise ValueError(f"Value `{value}` is not an IP address")
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._context_ip_cache = value
        self._context_ip_valid = True

    @property
    def context_mac(self) -> MDMType4.MAC:
        """
        Context MAC.

        :setter:
        :param value: The new value for :attr:`context_mac`.
        :type value: int | MDMType4.MAC
        :raises ValueError: If ``value`` is a string but is badly formatted.
        :raises TypeError: If ``value`` is an invalid type.
        """
        return MDMType4Parser.context_mac.fget(self)  # type: ignore

    @context_mac.setter
    def context_mac(self, value: int | MDMType4.MAC):
        if isinstance(value, int):
            self._pack_uint64(value, MDMType4.CONTEXT_MAC_INDEX)
            self._context_mac_valid = False
            value = self.context_mac
        elif isinstance(value, str):
            if ":" in value:
                write_mac_str_to_buffer(
                    value,
                    self,
                    MDMType4.CONTEXT_MAC_INDEX,
                )
            else:
                raise ValueError(f"Value `{value}` is not a MAC address")
        else:
            raise TypeError(f"Value `{value}` has invalid type: {type(value)}")
        self._context_mac_cache = value
        self._context_mac_valid = True

    @property
    def context_port(self) -> MDMType4.UDPPort:
        """
        Context port.

        :setter:
        :param value: The new value for :attr:`context_port`.
        :type value: MDMType4.UDPPort
        """
        return MDMType4Parser.context_port.fget(self)  # type: ignore

    @context_port.setter
    def context_port(self, value: MDMType4.UDPPort):
        self._pack_uint16(value, MDMType4.CONTEXT_PORT_INDEX)
        self._context_port_cache = value
        self._context_port_valid = True
