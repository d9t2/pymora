#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for ``pymora.mdm._type_7``."""

import pytest
import struct

import pymora

from test_mdm_header import create_default_mdm_header


def create_default_mdm_type_7(
    check_for_errors: bool = True,
) -> pymora.mdm.MDMType7Controller:
    ctrl = pymora.mdm.MDMType7Controller.new(check_for_errors=check_for_errors)
    ctrl.ack_code = 0
    ctrl.version = (2, 5)
    ctrl.sequence_number = 0
    ctrl.origination_id = 1
    ctrl.destination_id = 2
    ctrl.switch_group_resource_id = 1
    ctrl.user_id = 2
    ctrl.command_field_indicator = 1
    ctrl.command_mui = 1
    ctrl.command_ip = "192.168.0.1"
    ctrl.command_mac = "12:34:56:78:9a:bc"
    ctrl.command_port = 12345
    ctrl.context_field_indicator = 1
    ctrl.context_mui = 1
    ctrl.context_ip = "192.168.0.1"
    ctrl.context_mac = "12:34:56:78:9a:bc"
    ctrl.context_port = 12345
    return ctrl


def test_mdm_type_7_parser_get_properties():
    parser = pymora.mdm.MDMType7Parser(bytes(104), check_for_errors=False)
    print(str(parser))


def test_mdm_type_7_controller_get_properties():
    ctrl = pymora.mdm.MDMType7Controller.new(check_for_errors=False)
    print(str(ctrl))


def test_mdm_type_7_controller_set_properties():
    create_default_mdm_type_7()


def test_fail_mdm_type_7_controller_set_properties_bad_types():
    ctrl = pymora.mdm.MDMType7Controller.new()
    with pytest.raises(struct.error):
        ctrl.switch_group_resource_id = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.user_id = 2.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.command_field_indicator = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.command_mui = 1.0  # type: ignore
    with pytest.raises(TypeError):
        ctrl.command_ip = 1.0  # type: ignore
    with pytest.raises(TypeError):
        ctrl.command_mac = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.command_port = 12345.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.context_field_indicator = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.context_mui = 1.0  # type: ignore
    with pytest.raises(TypeError):
        ctrl.context_ip = 1.0  # type: ignore
    with pytest.raises(TypeError):
        ctrl.context_mac = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.context_port = 12345.0  # type: ignore


def test_mdm_type_7_validate_command_field_indicator():
    ctrl = pymora.mdm.MDMType7Controller.new()
    for cfi in pymora.mdm.MDMType7.FIELD_INDICATOR_VALID_VALUES:
        ctrl.command_field_indicator = cfi
        print(ctrl.command_field_indicator)


def test_fail_mdm_type_7_validate_command_field_indicator():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl2 = pymora.mdm.MDMType7Controller.new(check_for_errors=False)
    for i in (
        j
        for j in range(65536)
        if j not in pymora.mdm.MDMType7.FIELD_INDICATOR_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl.command_field_indicator = i  # type: ignore
        ctrl2.command_field_indicator = i  # type: ignore
        parser = pymora.mdm.MDMType7Parser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser.command_field_indicator)


def test_mdm_type_7_validate_command_ip():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl.command_field_indicator = 1
    ctrl.command_ip = "192.168.0.1"
    print(ctrl.command_ip)
    ctrl.invalidate_cache()
    ctrl.command_ip = (192 << 24) + (168 << 16) + (0 << 8) + (1 << 0)  # type: ignore  # noqa
    print(ctrl.command_ip)
    ctrl.invalidate_cache()
    ctrl.command_field_indicator = 2
    ctrl.command_ip = "1234:5678:9abc:def0:1234:5678:9abc:def0"
    print(ctrl.command_ip)
    ctrl.invalidate_cache()
    ctrl.command_ip = (
        (0x1234 << 56) + (0x5678 << 48) + (0x9abc << 40) + (0xdef0 << 32)  # type: ignore  # noqa
        + (0x1234 << 24) + (0x5678 << 16) + (0x9abc << 8) + (0xdef0 << 0)
    )
    print(ctrl.command_ip)


def test_fail_mdm_type_7_validate_command_ip():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl.command_field_indicator = 1
    with pytest.raises(ValueError):
        ctrl.command_ip = "192:168:0:1"
    with pytest.raises(ValueError):
        ctrl.command_ip = "192.168.0.1:12345"
    with pytest.raises(ValueError):
        ctrl.command_ip = "192.168.0.1.1"
    with pytest.raises(ValueError):
        ctrl.command_ip = "1234-5678-9abc-def0-1234-5678-9abc-def0"
    with pytest.raises(ValueError):
        ctrl.command_ip = "1234:5678:9abc:def0:1234:5678:9abc:def0:1234"


def test_mdm_type_7_validate_command_mac():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl.command_mac = "12:34:56:78:9a:bc"
    print(ctrl.command_mac)
    ctrl.invalidate_cache()
    ctrl.command_mac = (
        (0x12 << 40) + (0x34 << 32) + (0x56 << 24)  # type: ignore
        + (0x78 << 16) + (0x9a << 8) + (0xbc << 0))
    print(ctrl.command_mac)


def test_fail_mdm_type_7_validate_command_mac():
    ctrl = pymora.mdm.MDMType7Controller.new()
    with pytest.raises(ValueError):
        ctrl.command_mac = "12.34.56.78.9a.bc"
    with pytest.raises(ValueError):
        ctrl.command_mac = "12:34:56:78:9a:bc:de"


def test_mdm_type_7_validate_context_field_indicator():
    ctrl = pymora.mdm.MDMType7Controller.new()
    for cfi in pymora.mdm.MDMType7.FIELD_INDICATOR_VALID_VALUES:
        ctrl.context_field_indicator = cfi
        print(ctrl.context_field_indicator)


def test_fail_mdm_type_7_validate_context_field_indicator():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl2 = pymora.mdm.MDMType7Controller.new(check_for_errors=False)
    for i in (
        j
        for j in range(65536)
        if j not in pymora.mdm.MDMType7.FIELD_INDICATOR_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl.context_field_indicator = i  # type: ignore
        ctrl2.context_field_indicator = i  # type: ignore
        parser = pymora.mdm.MDMType7Parser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser.context_field_indicator)


def test_mdm_type_7_validate_context_ip():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl.context_field_indicator = 1
    ctrl.context_ip = "192.168.0.1"
    print(ctrl.context_ip)
    ctrl.invalidate_cache()
    ctrl.context_ip = (192 << 24) + (168 << 16) + (0 << 8) + (1 << 0)  # type: ignore  # noqa
    print(ctrl.context_ip)
    ctrl.invalidate_cache()
    ctrl.context_field_indicator = 2
    ctrl.context_ip = "1234:5678:9abc:def0:1234:5678:9abc:def0"
    print(ctrl.context_ip)
    ctrl.invalidate_cache()
    ctrl.context_ip = (
        (0x1234 << 56) + (0x5678 << 48) + (0x9abc << 40) + (0xdef0 << 32)  # type: ignore  # noqa
        + (0x1234 << 24) + (0x5678 << 16) + (0x9abc << 8) + (0xdef0 << 0)
    )
    print(ctrl.context_ip)


def test_fail_mdm_type_7_validate_context_ip():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl.context_field_indicator = 1
    with pytest.raises(ValueError):
        ctrl.context_ip = "192-168-0-1"
    with pytest.raises(ValueError):
        ctrl.context_ip = "192.168.0.1:12345"
    with pytest.raises(ValueError):
        ctrl.context_ip = "192.168.0.1.1"
    with pytest.raises(ValueError):
        ctrl.context_ip = "1234-5678-9abc-def0-1234-5678-9abc-def0"
    with pytest.raises(ValueError):
        ctrl.context_ip = "1234:5678:9abc:def0:1234:5678:9abc:def0:1234"


def test_mdm_type_7_validate_context_mac():
    ctrl = pymora.mdm.MDMType7Controller.new()
    ctrl.context_mac = "12:34:56:78:9a:bc"
    print(ctrl.context_mac)
    ctrl.invalidate_cache()
    ctrl.context_mac = (
        (0x12 << 40) + (0x34 << 32) + (0x56 << 24)  # type: ignore
        + (0x78 << 16) + (0x9a << 8) + (0xbc << 0))
    print(ctrl.context_mac)


def test_fail_mdm_type_7_validate_context_mac():
    ctrl = pymora.mdm.MDMType7Controller.new()
    with pytest.raises(ValueError):
        ctrl.context_mac = "12.34.56.78.9a.bc"
    with pytest.raises(ValueError):
        ctrl.context_mac = "12:34:56:78:9a:bc:de"


def test_mdm_type_7_controller_inverse():
    ctrl = create_default_mdm_type_7(check_for_errors=False)
    parser = pymora.mdm.MDMType7Parser(ctrl._buffer, check_for_errors=False)
    if parser != ctrl:
        raise RuntimeError(
            "Parser and controller are different. \n"
            f"Parser:\n{parser}\n"
            f"Controller:\n{ctrl}"
        )


def test_mdm_type_7_controller_shared_buffer():
    ctrl = create_default_mdm_type_7(check_for_errors=False)
    ctrl2 = pymora.mdm.MDMType7Controller(ctrl._buffer, check_for_errors=False)
    ctrl2.switch_group_resource_id = 2
    ctrl2.user_id = 3
    ctrl2.command_field_indicator = 2
    ctrl2.command_mui = 2
    ctrl2.command_ip = "192.168.0.2"
    ctrl2.command_mac = "12:34:56:78:9a:bd"
    ctrl2.command_port = 12346
    ctrl2.context_field_indicator = 2
    ctrl2.context_mui = 2
    ctrl2.context_ip = "192.168.0.2"
    ctrl2.context_mac = "12:34:56:78:9a:bd"
    ctrl2.context_port = 12346
    ctrl.invalidate_cache()
    if ctrl2 != ctrl:
        raise RuntimeError(
            "Controllers are different, but should be the same \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )


def test_mdm_type_7_controller_inequality():
    ctrl = create_default_mdm_type_7()
    ctrl2 = create_default_mdm_header()
    if ctrl == ctrl2:
        raise RuntimeError(
            "Controllers are the same, but should be different \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )
