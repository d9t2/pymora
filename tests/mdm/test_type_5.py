#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for ``pymora.mdm._type_5``."""

import pytest
import struct

import pymora

from test_mdm_header import create_default_mdm_header


def create_default_mdm_type_5(
    check_for_errors: bool = True,
) -> pymora.mdm.MDMType5Controller:
    ctrl = pymora.mdm.MDMType5Controller.new(check_for_errors=check_for_errors)
    ctrl.ack_code = 0
    ctrl.version = (2, 5)
    ctrl.sequence_number = 0
    ctrl.origination_id = 1
    ctrl.destination_id = 2
    ctrl.alert_type = 0
    ctrl.port_id = 1
    ctrl.operational_parameter = 1
    ctrl.operational_state = 1
    ctrl.parameter_condition = 0x0800
    return ctrl


def test_mdm_type_5_parser_get_properties():
    parser = pymora.mdm.MDMType5Parser(
        bytes(pymora.mdm.MDMType5.BYTES_IN_PACKET),
        check_for_errors=False,
    )
    print(str(parser))


def test_mdm_type_5_controller_get_properties():
    ctrl = pymora.mdm.MDMType5Controller.new(check_for_errors=False)
    print(str(ctrl))


def test_mdm_type_5_controller_set_properties():
    create_default_mdm_type_5()


def test_fail_mdm_type_5_controller_set_properties_bad_types():
    ctrl = pymora.mdm.MDMType5Controller.new()
    with pytest.raises(TypeError):
        ctrl.alert_type = 0.0  # type: ignore
    with pytest.raises(TypeError):
        ctrl.port_id = 1.0  # type: ignore
    with pytest.raises(TypeError):
        ctrl.operational_parameter = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.operational_state = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.parameter_condition = 1.0  # type: ignore


def test_mdm_type_5_validate_alert_type():
    ctrl = pymora.mdm.MDMType5Controller.new()
    for at in pymora.mdm.MDMType5.ALERT_TYPE_VALID_VALUES:
        ctrl.alert_type = at
        print(ctrl.alert_type)


def test_fail_mdm_type_5_validate_alert_type():
    ctrl = pymora.mdm.MDMType5Controller.new()
    ctrl2 = pymora.mdm.MDMType5Controller.new(check_for_errors=False)
    for i in (
        j
        for j in range(4)
        if j not in pymora.mdm.MDMType5.ALERT_TYPE_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl.alert_type = i  # type: ignore
        ctrl2.alert_type = i  # type: ignore
        parser = pymora.mdm.MDMType5Parser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser.alert_type)


def test_mdm_type_5_validate_port_id():
    ctrl = pymora.mdm.MDMType5Controller.new()
    for pid in range(128):
        ctrl.port_id = pid
        print(ctrl.port_id)


def test_mdm_type_5_validate_operational_parameter():
    ctrl = pymora.mdm.MDMType5Controller.new()
    for op in pymora.mdm.MDMType5.OPERATIONAL_PARAMETER_VALID_VALUES:
        ctrl.operational_parameter = op
        print(ctrl.operational_parameter)


def test_fail_mdm_type_5_validate_operational_parameter():
    ctrl = pymora.mdm.MDMType5Controller.new()
    for op in pymora.mdm.MDMType5.OPERATIONAL_PARAMETER_VALID_VALUES:
        with pytest.raises(ValueError):
            ctrl.operational_parameter = 0x00400000 ^ op  # type: ignore
        ctrl.check_for_errors = False
        ctrl.operational_parameter = 0x00400000 ^ op  # type: ignore
        ctrl._operational_parameter_valid = False
        ctrl.check_for_errors = True
        with pytest.raises(ValueError):
            print(ctrl.operational_parameter)


def test_mdm_type_5_validate_parameter_condition():
    ctrl = pymora.mdm.MDMType5Controller.new()
    for pc in pymora.mdm.MDMType5.PARAMETER_CONDITION_VALID_VALUES:
        ctrl.parameter_condition = pc
        print(ctrl.parameter_condition)


def test_fail_mdm_type_5_validate_parameter_condition():
    ctrl = pymora.mdm.MDMType5Controller.new()
    ctrl2 = pymora.mdm.MDMType5Controller.new(check_for_errors=False)
    for i in (
        j
        for j in range(65536)
        if j not in pymora.mdm.MDMType5.PARAMETER_CONDITION_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl.parameter_condition = i  # type: ignore
        ctrl2.parameter_condition = i  # type: ignore
        parser = pymora.mdm.MDMType5Parser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser.parameter_condition)


def test_mdm_type_5_controller_inverse():
    ctrl = create_default_mdm_type_5(check_for_errors=False)
    parser = pymora.mdm.MDMType5Parser(ctrl._buffer, check_for_errors=False)
    if parser != ctrl:
        raise RuntimeError(
            "Parser and controller are different. \n"
            f"Parser:\n{parser}\n"
            f"Controller:\n{ctrl}"
        )


def test_mdm_type_5_controller_shared_buffer():
    ctrl = create_default_mdm_type_5(check_for_errors=False)
    ctrl2 = pymora.mdm.MDMType5Controller(ctrl._buffer, check_for_errors=False)
    ctrl2.alert_type = 1
    ctrl2.port_id = 2
    ctrl2.operational_parameter = 2
    ctrl2.operational_state = 2
    ctrl2.parameter_condition = 1
    ctrl.invalidate_cache()
    if ctrl2 != ctrl:
        raise RuntimeError(
            "Controllers are different, but should be the same \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )


def test_mdm_type_5_controller_inequality():
    ctrl = create_default_mdm_type_5()
    ctrl2 = create_default_mdm_header()
    if ctrl == ctrl2:
        raise RuntimeError(
            "Controllers are the same, but should be different \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )
