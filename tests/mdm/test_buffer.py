#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for ``pymora.mdm._buffer``."""

import pytest

import pymora


def test_buffer_parser_from_bytes():
    pymora.mdm._buffer.BufferParser(bytes(10))


def test_buffer_parser_from_bytearray():
    pymora.mdm._buffer.BufferParser(bytearray(10))


def test_fail_buffer_parser_from_list():
    with pytest.raises(TypeError):
        pymora.mdm._buffer.BufferParser(list([0] * 10))  # type: ignore


def test_buffer_parser_getitem_access():
    buffer = bytearray(range(10))
    parser = pymora.mdm._buffer.BufferParser(buffer)
    for i in range(10):
        if buffer[i] != parser[i]:
            pytest.fail("Fail: BufferParser.__getitem__")


def test_fail_buffer_parser_getitem_access_out_of_range():
    parser = pymora.mdm._buffer.BufferParser(bytearray(10))
    with pytest.raises(IndexError):
        parser[10]


def test_buffer_parser_str():
    parser = pymora.mdm._buffer.BufferParser(bytearray(10))
    print(str(parser))


def test_fail_buffer_controller_from_bytes():
    with pytest.raises(TypeError):
        pymora.mdm._buffer.BufferController(bytes(10))  # type: ignore


def test_buffer_controller_from_bytearray():
    pymora.mdm._buffer.BufferController(bytearray(10))


def test_buffer_controller_from_new():
    pymora.mdm._buffer.BufferController._new(10)


def test_buffer_controller_setitem_access():
    ctrl = pymora.mdm._buffer.BufferController._new(10)
    for i in range(10):
        ctrl[i] = i


def test_fail_buffer_controller_setitem_access_out_of_range():
    ctrl = pymora.mdm._buffer.BufferController._new(10)
    with pytest.raises(IndexError):
        ctrl[10] = 0


def test_buffer_controller_setitem_range():
    ctrl = pymora.mdm._buffer.BufferController._new(1)
    for i in range(256):
        ctrl[0] = i


def test_fail_buffer_controller_setitem_low_range():
    ctrl = pymora.mdm._buffer.BufferController._new(1)
    with pytest.raises(ValueError):
        ctrl[0] = -1


def test_fail_buffer_controller_setitem_high_range():
    ctrl = pymora.mdm._buffer.BufferController._new(1)
    with pytest.raises(ValueError):
        ctrl[0] = 256


def test_buffer_controller_inverse():
    ctrl = pymora.mdm._buffer.BufferController._new(10)
    for i in range(10):
        ctrl[i] = i
    parser = pymora.mdm._buffer.BufferParser(ctrl._buffer)
    if ctrl != parser:
        raise RuntimeError(
            "Controller and parser are unequal with empty buffers. \n"
            f"Controller:\n{ctrl}\n"
            f"Parser:\n{parser}"
        )


def test_buffer_controller_shared_buffer():
    ctrl = pymora.mdm._buffer.BufferController._new(10)
    for i in range(10):
        ctrl[i] = i
    ctrl2 = pymora.mdm._buffer.BufferController(ctrl._buffer)
    for i in range(10):
        ctrl2[i] = i + 1
    if ctrl2 != ctrl:
        raise RuntimeError(
            "Controllers are different. \n"
            f"Controller 2:\n{ctrl2}\n"
            f"Controller:\n{ctrl}"
        )
