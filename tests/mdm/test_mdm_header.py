#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for ``pymora.mdm._mdm_header``."""

import pytest
import struct

import pymora


def create_default_mdm_header(
    check_for_errors: bool = True,
) -> pymora.mdm.MDMHeaderController:
    ctrl = pymora.mdm.MDMHeaderController._new(
        16,
        check_for_errors=check_for_errors,
    )
    ctrl.ack_code = 0
    ctrl.version = (2, 5)
    ctrl.sequence_number = 0
    ctrl.origination_id = 1
    ctrl.destination_id = 2
    ctrl._mdm_type = 2
    ctrl._number_of_messages = 0
    return ctrl


def test_mdm_header_parser_get_properties():
    parser = pymora.mdm.MDMHeaderParser(bytes(16), check_for_errors=False)
    print(str(parser))


def test_mdm_header_controller_get_properties():
    ctrl = pymora.mdm.MDMHeaderController._new(16, check_for_errors=False)
    print(str(ctrl))


def test_mdm_header_controller_set_properties():
    create_default_mdm_header()


def test_fail_mdm_header_controller_set_properties_bad_types():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    with pytest.raises(ValueError):
        ctrl._preamble = "MDM"  # type: ignore
    with pytest.raises(TypeError):
        ctrl.ack_code = 0.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.version = (2.0, 5.0)  # type: ignore
    with pytest.raises(struct.error):
        ctrl.sequence_number = 0.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.origination_id = 1.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl.destination_id = 2.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl._mdm_type = 2.0  # type: ignore
    with pytest.raises(struct.error):
        ctrl._mdm_type = 2
        ctrl._number_of_messages = 0.0  # type: ignore


def test_mdm_header_validate_preamble():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    ctrl._preamble = pymora.mdm.MDMHeader.PREAMBLE_VALID_VALUE
    print(ctrl._preamble)


def test_fail_mdm_header_validate_preamble():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    with pytest.raises(ValueError):
        ctrl._preamble = b"AAA"  # type: ignore
    ctrl2 = pymora.mdm.MDMHeaderController._new(16, check_for_errors=False)
    ctrl2._preamble = b"AAA"  # type: ignore
    parser = pymora.mdm.MDMHeaderParser(ctrl2._buffer)
    with pytest.raises(ValueError):
        print(parser._preamble)


def test_mdm_header_validate_ack_code():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    for ack_code in pymora.mdm.MDMHeader.ACK_CODE_VALID_VALUES:
        ctrl.ack_code = ack_code
        print(ctrl.ack_code)


def test_fail_mdm_header_validate_ack_code():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    ctrl2 = pymora.mdm.MDMHeaderController._new(16, check_for_errors=False)
    for i in (
        j
        for j in range(256)
        if j not in pymora.mdm.MDMHeader.ACK_CODE_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl.ack_code = i  # type: ignore
        ctrl2.ack_code = i  # type: ignore
        parser = pymora.mdm.MDMHeaderParser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser.ack_code)


def test_mdm_header_validate_version():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    for version in pymora.mdm.MDMHeader.VERSION_VALID_VALUES:
        ctrl.version = version
        print(ctrl.version)


def test_fail_mdm_header_validate_version():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    ctrl2 = pymora.mdm.MDMHeaderController._new(16, check_for_errors=False)
    majors = (maj for maj, min in pymora.mdm.MDMHeader.VERSION_VALID_VALUES)
    minors = (min for maj, min in pymora.mdm.MDMHeader.VERSION_VALID_VALUES)
    bad_majors = (j for j in range(256) if j not in majors)
    bad_minors = (j for j in range(256) if j not in minors)
    for major in bad_majors:
        for minor in bad_minors:
            with pytest.raises(ValueError):
                ctrl.version = (major, minor)
            ctrl2.version = (major, minor)
            parser = pymora.mdm.MDMHeaderParser(ctrl2._buffer)
            with pytest.raises(ValueError):
                print(parser.version)


def test_mdm_header_validate_mdm_type():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    for mdm_type in pymora.mdm.MDMHeader.MDM_TYPE_VALID_VALUES:
        ctrl._mdm_type = mdm_type
        print(ctrl._mdm_type)


def test_fail_mdm_header_validate_mdm_type():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    ctrl2 = pymora.mdm.MDMHeaderController._new(16, check_for_errors=False)
    for i in (
        j
        for j in range(65536)
        if j not in pymora.mdm.MDMHeader.MDM_TYPE_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl._mdm_type = i  # type: ignore
        ctrl2._mdm_type = i  # type: ignore
        parser = pymora.mdm.MDMHeaderParser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser._mdm_type)


def test_mdm_header_validate_number_of_messages():
    ctrl = pymora.mdm.MDMHeaderController._new(16)
    for mt, nom in pymora.mdm.MDMHeader.NUMBER_OF_MESSAGES_VALID_VALUES.items():
        ctrl._mdm_type = mt
        ctrl._number_of_messages = nom
        print(ctrl._number_of_messages)


# This is programmatically creating test cases for each MDM type so the tests
# can be automatically produced and run in parallel
for mdm_type in pymora.mdm.MDMHeader.NUMBER_OF_MESSAGES_VALID_VALUES.keys():

    def func(mdm_type=mdm_type):
        ctrl = pymora.mdm.MDMHeaderController._new(16)
        ctrl._mdm_type = mdm_type
        ctrl2 = pymora.mdm.MDMHeaderController._new(16, check_for_errors=False)
        ctrl2._mdm_type = mdm_type
        for i in (
            j
            for j in range(65536)
            if j != pymora.mdm.MDMHeader.NUMBER_OF_MESSAGES_VALID_VALUES[mdm_type]
        ):
            with pytest.raises(ValueError):
                ctrl._number_of_messages = i  # type: ignore
            ctrl2._number_of_messages = i  # type: ignore
            parser = pymora.mdm.MDMHeaderParser(ctrl2._buffer)
            with pytest.raises(ValueError):
                print(parser._number_of_messages)

    func.__name__ = f"test_fail_mdm_header_validate_number_of_messages_{mdm_type}"  # noqa
    globals()[func.__name__] = func


def test_mdm_header_controller_inverse():
    ctrl = create_default_mdm_header(check_for_errors=False)
    parser = pymora.mdm.MDMHeaderParser(ctrl._buffer, check_for_errors=False)
    if parser != ctrl:
        raise RuntimeError(
            "Parser and controller are different. \n"
            f"Parser:\n{parser}\n"
            f"Controller:\n{ctrl}"
        )


def test_mdm_header_controller_shared_buffer():
    ctrl = create_default_mdm_header(check_for_errors=False)
    ctrl2 = pymora.mdm.MDMHeaderController(ctrl._buffer, check_for_errors=False)
    ctrl2._preamble = b"MDN"  # type: ignore
    ctrl2.ack_code = 1
    ctrl2.version = (2, 4)
    ctrl2.sequence_number = 1
    ctrl2.origination_id = 2
    ctrl2.destination_id = 3
    ctrl2._mdm_type = 4
    ctrl2._number_of_messages = 0
    ctrl.invalidate_cache()
    if ctrl2 != ctrl:
        raise RuntimeError(
            "Controllers are different, but should be the same \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )


def test_mdm_header_controller_inequality():
    ctrl = create_default_mdm_header()
    ctrl2 = pymora.mdm._buffer.BufferController._new(16)
    if ctrl == ctrl2:
        raise RuntimeError(
            "Controllers are the same, but should be different \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )
