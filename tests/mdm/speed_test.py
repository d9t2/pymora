#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Speed test."""

import timeit


setup = """
import pymora

from test_type_2 import create_default_mdm_type_2
from test_type_3 import create_default_mdm_type_3
from test_type_4 import create_default_mdm_type_4
from test_type_5 import create_default_mdm_type_5
from test_type_6 import create_default_mdm_type_6
from test_type_7 import create_default_mdm_type_7

def run():
    # Creation
    ctrllr_2 = create_default_mdm_type_2()
    ctrllr_3 = create_default_mdm_type_3()
    ctrllr_4 = create_default_mdm_type_4()
    ctrllr_5 = create_default_mdm_type_5()
    ctrllr_6 = create_default_mdm_type_6()
    ctrllr_7 = create_default_mdm_type_7()
    # Parsing
    pymora.mdm.MDMType2Parser(ctrllr_2._buffer).to_dictionary()
    pymora.mdm.MDMType3Parser(ctrllr_3._buffer).to_dictionary()
    pymora.mdm.MDMType4Parser(ctrllr_4._buffer).to_dictionary()
    pymora.mdm.MDMType5Parser(ctrllr_5._buffer).to_dictionary()
    pymora.mdm.MDMType6Parser(ctrllr_6._buffer).to_dictionary()
    pymora.mdm.MDMType7Parser(ctrllr_7._buffer).to_dictionary()
"""


if __name__ == "__main__":

    number = 1000
    time = timeit.timeit(stmt="run()", setup=setup, number=number)
    number_in_one_second = int((1 / time) * number)
    time = timeit.timeit(stmt="run()", setup=setup, number=number)
    print(f"In one second, {number_in_one_second} controller / parser pairs "
          "were evaluated")
