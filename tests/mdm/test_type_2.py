#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for ``pymora.mdm._type_2``."""

import pytest

import pymora

from test_mdm_header import create_default_mdm_header


def create_default_mdm_type_2(
    check_for_errors: bool = True,
) -> pymora.mdm.MDMType2Controller:
    ctrl = pymora.mdm.MDMType2Controller.new(check_for_errors=check_for_errors)
    ctrl.ack_code = 1
    ctrl.version = (2, 5)
    ctrl.sequence_number = 0
    ctrl.origination_id = 1
    ctrl.destination_id = 2
    return ctrl


def test_mdm_type_2_parser_get_properties():
    parser = pymora.mdm.MDMType2Parser(
        bytes(pymora.mdm.MDMType2.BYTES_IN_PACKET),
        check_for_errors=False,
    )
    print(str(parser))


def test_mdm_type_2_controller_get_properties():
    ctrl = pymora.mdm.MDMType2Controller.new(check_for_errors=False)
    print(str(ctrl))


def test_mdm_type_2_controller_set_properties():
    create_default_mdm_type_2()


def test_mdm_type_2_validate_ack_code():
    ctrl = pymora.mdm.MDMType2Controller.new()
    for ack_code in pymora.mdm.MDMType2.ACK_CODE_VALID_VALUES:
        ctrl.ack_code = ack_code
        print(ctrl.ack_code)


def test_fail_mdm_type_2_validate_ack_code():
    ctrl = pymora.mdm.MDMType2Controller.new()
    ctrl2 = pymora.mdm.MDMType2Controller.new(check_for_errors=False)
    for i in (
        j
        for j in range(256)
        if j not in pymora.mdm.MDMType2.ACK_CODE_VALID_VALUES
    ):
        with pytest.raises(ValueError):
            ctrl.ack_code = i  # type: ignore
        ctrl2.ack_code = i  # type: ignore
        parser = pymora.mdm.MDMType2Parser(ctrl2._buffer)
        with pytest.raises(ValueError):
            print(parser.ack_code)


def test_mdm_type_2_controller_inverse():
    ctrl = create_default_mdm_type_2(check_for_errors=False)
    parser = pymora.mdm.MDMType2Parser(ctrl._buffer, check_for_errors=False)
    if parser != ctrl:
        raise RuntimeError(
            "Parser and controller are different. \n"
            f"Parser:\n{parser}\n"
            f"Controller:\n{ctrl}"
        )


def test_mdm_type_2_controller_shared_buffer():
    ctrl = create_default_mdm_type_2(check_for_errors=False)
    ctrl2 = pymora.mdm.MDMType2Controller(ctrl._buffer, check_for_errors=False)
    ctrl2.ack_code = 1
    ctrl.invalidate_cache()
    if ctrl2 != ctrl:
        raise RuntimeError(
            "Controllers are different, but should be the same \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )


def test_mdm_header_controller_inequality():
    ctrl = create_default_mdm_type_2()
    ctrl2 = create_default_mdm_header()
    if ctrl == ctrl2:
        raise RuntimeError(
            "Controllers are the same, but should be different \n"
            f"Controller:\n{ctrl}\n"
            f"Controller 2:\n{ctrl2}"
        )
