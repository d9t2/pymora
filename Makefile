# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Variables
# # @
at ?= @
# # Directory of this Makefile
makefile_dir = $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

# Rules
.PHONY: default
default:

.PHONY: all
all: mypy tests docs

.PHONY: clean
clean:
	$(at)# Python bytecode
	$(at)find $(makefile_dir) -name "__pycache__" -type d -print0 | \
		xargs -0 /bin/rm -rf
	$(at)# Analysis
	$(at)rm -rf $(makefile_dir)/.mypy_cache
	$(at)# Documentation
	$(at)rm -rf $(makefile_dir)/docs/build
	$(at)# Tests and Coverage
	$(at)rm -rf $(makefile_dir)/.coverage
	$(at)rm -rf $(makefile_dir)/.pytest_cache
	$(at)rm -rf $(makefile_dir)/htmlcov

.PHONY: docs
docs:
	$(at)sphinx-build \
		-M html \
		$(makefile_dir)/docs/source \
		$(makefile_dir)/docs/build

.PHONY: mypy
mypy:
	$(at)mypy \
		--check-untyped-defs \
		--warn-redundant-casts \
		--warn-unused-ignores \
		--warn-return-any \
		--warn-unreachable \
		--allow-redefinition \
		--strict-equality \
		--strict-concatenate \
		$$(find $(makefile_dir) \
			-name "*.py" -type f \
		)

pytest_omit = "tests/*/test_*.py,tests/*/*_test.py"

.PHONY: tests
tests:
	$(at)pytest tests/ \
		--capture=fd \
		-n auto \
		--cov=$(makefile_dir) \
		--cov-report= \
		--verbose
	$(at)coverage html --omit=$(pytest_omit)
	$(at)coverage report -m --omit=$(pytest_omit)
	$(at)$(makefile_dir)/tests/mdm/speed_test.py
