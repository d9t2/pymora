# PyMORA

A python library providing Modular Open RF Architecture (MORA) utilities.

## Introduction: `pymora.mdm`

`pymora.mdm` aspires to make it easier for a developer to quickly create and
parse MDMs.

### `pymora.mdm`: Parse

Lets imagine that your program has pulled a UDP Packet through the `socket`
module.

You'll be left with a `bytes` object that you have to parse and validate as
you require.

If you want to create an MDM Type 3 parser over that data, you can do:

```python
import pymora
parser = pymora.mdm.MDMType3Parser(data)
```

Then, to get the value of the "Time of Day" field:

```python
parser.time_of_day
```

This takes the relevant bytes from the buffer, assembles the integer time of
day, then uses the `time` module to provide a user friendly `struct_time` in
UTC. This only happens once; a second access will use a cached value rather
than reassembling the value from the buffer again.

`pymora.mdm` chooses the most user friendly format to provide to the user from
the "getter". For example, IP and MAC addresses are given as strings.

The parser will validate where possible. For example:

* When a parser is created it checks that the `bytes` object is big enough
  for the requested packet.

* When "getters" are used for enumerated fields, the assembled value will be
  checked against the known good list of options.

### `pymora.mdm`:  Create

If you want to create an MDM Type 3 controller, you can do:

```python
import pymora
ctrllr = pymora.mdm.MDMType3Controller.new()
```

Then, to set the value of the "Time of Day" field:

```python
import time
ctrllr.time_of_day = time.gmtime()
```

The "setter" then takes the provided value, validates it where suitable,
converts it to an integer, and writes it into the internal bytes buffer. This
"setter" also sets the cache, so the equivalent "getter" is free.

`pymora.mdm` tries to give you as many valid setting options as reasonably
possible. For example:

* Type 3: `time_of_day` can be set using `int`, `float`, or `struct_time`

  * `float`s are cast to `int`.
  * `struct_time`s can also be non-UTC and they will be converted to UTC before
    caching them.

* Type 4: `command_ip` can be set using `int` or `str`

  * `str`s of the format `"A.B.C.D"` (each part in base 10) for IPV4.
  * `str`s of the format `"E:F:G:H:I:J:K:L"` (each part in base 16) for IPV6.
  * `int`s are interpreted according to `command_field_indicator` as to whether
    they should be IPV4 or IPV6.

The controller will validate where possible. For example:

  * When "setters" are used for enumerated fields, the provided value will be
    checked against the known good list of options.

Once all of your "setters" are called, the buffer is then available to be used
as you please. For example, to send the packet using the `socket` module:

```python
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.sendto(ctrllr._buffer, ("192.168.0.1", 12345))
```

## Copyright

MIT License

Copyright (c) 2023 Dominic Adam Walters

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
